CREATE OR REPLACE PROCEDURE `S_CLM_Event_Execution`(IN OnDemandEventId BIGINT(20),IN RUN_MODE Varchar(10), OUT no_of_customers BIGINT(20),OUT vSuccess int (4), OUT vErrMsg Text)
BEGIN
   /* Version : Campaign UI 3.0 20230704 */
DECLARE vEvent_Limit_1 varchar(16);

DECLARE WATERFALL_COMPLIANCE int default 99;
DECLARE iPrintCreative int default 0;
DECLARE WATERFALL_CMAB_PERFORMANCE_BASED int default 100;
DECLARE vRecommendation_Filter_Logic varchar(2048);
Declare Event_Condition_SVOC_1, Event_Condition_Bill_1,Communication_CoolOff_1 text;
DECLARE vCommunication_CoolOff int;
DECLARE V_Offer_Code_1 varchar(20);
DECLARE  Exe_ID, Event_ID_cur1_1, Communication_Template_ID_cur1_1, Reminder_Parent_TriggerId_cur1_1, Reminder_Days_cur1_1,vGoodTime_1,vEvent_CoolOff_Days_1 int;
DECLARE vCommunication_Channel_1 VARCHAR(256);
DECLARE vTimeSlot_Id varchar(20); 
DECLARE vCLMSegment_Id bigint(20);
DECLARE cov1,cov2 BIGINT DEFAULT 0;
DECLARE v_holdout_flag varchar(64) default 'INITIAIZE';

DECLARE done1,done2 INT DEFAULT FALSE;
DECLARE cur1 cursor for 
	SELECT 	em.ID,
			em.Replaced_Query, 
            em.Replaced_Query_Bill,
            em.Reminder_Parent_Event_Id, 
			em.Reminder_Days , 
            em.Communication_Template_ID,
            ct.Offer_Code,
            ifnull(Event_Limit,10000000),
            ifnull(ct.Communication_Cooloff,0),
            em.Is_Good_Time,
            ct.Communication_Channel,
            ct.Recommendation_Filter_Logic,
			ct.Timeslot_Id,
            em.CLMSegmentId
	FROM Event_Master em
    JOIN Communication_Template ct on ct.ID=em.Communication_Template_ID
    WHERE  
     ct.Template<>'' AND ct.Template is not NULL and ct.Template<>'<SOLUS_PFIELD></SOLUS_PFIELD>' and
		CASE
         WHEN OnDemandEventId = '' AND RUN_MODE='TEST' THEN em.ID in (Select ID from Event_Master where State='Testing') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID))
         WHEN OnDemandEventId = '' AND RUN_MODE='RUN' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID))
		 WHEN OnDemandEventId <> '' AND RUN_MODE='TEST' THEN em.ID in (Select ID from Event_Master where State='Testing') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
         WHEN OnDemandEventId <> '' AND RUN_MODE='RUN' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
	     WHEN OnDemandEventId = '' AND RUN_MODE='' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) 
         WHEN OnDemandEventId <> '' AND RUN_MODE='' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
         WHEN OnDemandEventId = '' AND RUN_MODE='TEST_ALL' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID))
         WHEN OnDemandEventId <> '' AND RUN_MODE='TEST_ALL' THEN em.ID in (Select ID from Event_Master where State='Enabled') and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID)) AND em.ID =OnDemandEventId
         WHEN OnDemandEventId = '' AND RUN_MODE='CMAB' THEN em.ID in (Select ID from Event_Master where State='Enabled' AND Follows_Waterfall=100) and (curdate() between F_Get_Std_Date(em.Start_Date_ID) and F_Get_Std_Date(em.End_Date_ID))
		 END
ORDER BY em.Event_Sequence_Waterfall,em.Execution_Sequence;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done1 = TRUE;

SET done1 = FALSE;
SET SQL_SAFE_UPDATES=0;
SET foreign_key_checks=0;
SELECT CAST(REPLACE(CURRENT_DATE(), '-', '') AS UNSIGNED) INTO @Event_Execution_Date_ID;
set @vStart_Cnt = 0;set @vStart_CntCust=0; set @vEnd_Cnt=0; set @vEnd_CntCust=0; set @vBatchSizeBills=100000;

insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
	values ('S_CLM_Event_Execution', CONCAT_WS('',CASE WHEN RUN_MODE='' THEN 'RUN' ELSE RUN_MODE END,':',
    CASE WHEN OnDemandEventId='' 
    THEN 'ALL EVENTS' ELSE OnDemandEventId END),now(),null,'Started', 181021);


SELECT 
    CONCAT_WS('',
            'S_CLM_Event_Execution Started .. ',
            NOW()) AS '';
SELECT '  VERSION : Solus 2.1 20221124 ' AS '';

TRUNCATE Event_Execution;
TRUNCATE Event_Execution_Rejected;

SELECT 
    IFNULL(Value, 'Y')
INTO @Truncate_EEC FROM
    M_Config
WHERE
    Name = 'Truncate_Event_Execution_Current_Run';

IF @Truncate_EEC IS NULL OR @Truncate_EEC = ''
THEN 
	SET @Truncate_EEC = 'Y';
END IF;

IF @Truncate_EEC = 'N'
THEN
	DELETE FROM Event_Execution_Current_Run 
    WHERE Event_Execution_Date_Id <> @Event_Execution_Date_ID;
ELSE
	TRUNCATE Event_Execution_Current_Run;
    
END IF;

SET Exe_ID = F_Get_Execution_Id();
SELECT 
    Customer_id
INTO @Rec_Cnt FROM
    V_CLM_Customer_One_View
ORDER BY Customer_id DESC
LIMIT 1;

SET @Rec_Cnt=ifnull(@Rec_Cnt,0);
SELECT 
    CONCAT_WS('',
            ' Max CustomerId to Be Processed  : ',
            @Rec_Cnt) AS '';


SELECT 
    `Value`
INTO @V_Tenant_Name FROM
    M_Config
WHERE
    `Name` = 'Tenant_Name';
SELECT 
    CONCAT_WS('',
            'Schema Name : ',
            IFNULL(@V_Tenant_Name, 'NOT CONFIGURED')) AS '';


IF RUN_MODE <> 'CMAB'
THEN 

                
    SELECT 
        MIN(Value)
    INTO @Commu_CoolOff_Days FROM
        M_Config
    WHERE
        `Name` = 'Communication_CoolOff_Days';
SELECT 
    CONCAT_WS('',
            'Global Communication Cool Off  : ',
            IFNULL(@Commu_CoolOff_Days, 'NOT CONFIGURED')) AS '';
                
SELECT 
    MIN(Value)
INTO @SMS_Commu_CoolOff_Days FROM
    M_Config
WHERE
    `Name` = 'SMS_Communication_CoolOff_Days';
SELECT 
    CONCAT_WS('',
            'SMS Channel Communication Cool Off  : ',
            IFNULL(@SMS_Commu_CoolOff_Days,
                    'NOT CONFIGURED')) AS '';
                
SELECT 
    MIN(Value)
INTO @Email_Commu_CoolOff_Days FROM
    M_Config
WHERE
    `Name` = 'Email_Communication_CoolOff_Days';
SELECT 
    CONCAT_WS('',
            'Email Channel Communication Cool Off  : ',
            IFNULL(@Email_Commu_CoolOff_Days,
                    'NOT CONFIGURED')) AS '';
                
SELECT 
    MIN(Value)
INTO @PN_Commu_CoolOff_Days FROM
    M_Config
WHERE
    `Name` = 'PN_Communication_CoolOff_Days';
SELECT 
    CONCAT_WS('',
            'PN Channel Communication Cool Off  : ',
            IFNULL(@PN_Commu_CoolOff_Days, 'NOT CONFIGURED')) AS '';

SELECT 
    MIN(Value)
INTO @WhatsApp_Commu_CoolOff_Days FROM
    M_Config
WHERE
    `Name` = 'WhatsApp_Communication_CoolOff_Days';
SELECT 
    CONCAT_WS('',
            'WhatsApp Channel Communication Cool Off  : ',
            IFNULL(@WhatsApp_Commu_CoolOff_Days,
                    'NOT CONFIGURED')) AS '';

END IF;

SELECT 
    IFNULL(`Value`, 100000)
INTO @vBatchSizeCust FROM
    M_Config
WHERE
    `Name` = 'Event_Execution_Batch_Size';
IF @vBatchSizeCust < 1
THEN SET @vBatchSizeCust = 100000;
END IF;

SELECT 
    IFNULL(`Value`, 10000000)
INTO @Overall_Limit FROM
    M_Config
WHERE
    `Name` = 'CLM_Event_Limit';
IF @Overall_Limit < 1
THEN SET @Overall_Limit = 10000000;
END IF;
SELECT 
    CONCAT_WS('',
            'Global Throttling Limit (Max Communications) : ',
            IFNULL(@Overall_Limit, 'NOT CONFIGURED')) AS '';

SELECT 
    IFNULL(`Value`, 'Stories')
INTO @Reco_Strategy FROM
    M_Config
WHERE
    `Name` = 'Recommendation_UseCase_Method';
SELECT 
    CONCAT_WS('',
            'Recommendation Strategy   : ',
            IFNULL(@Reco_Strategy, 'NOT CONFIGURED')) AS '';

SELECT 
    IFNULL(`Value`, 'Customer_Batch')
INTO @File_Generation_Type FROM
    M_Config
WHERE
    `Name` = 'File_Generation_Method';
SELECT 
    CONCAT_WS('',
            'File Generation Method   : ',
            IFNULL(@File_Generation_Type, 'NOT CONFIGURED')) AS '';
SELECT 
    IFNULL(`Value`, 'AUTO')
INTO @File_Generation_BatchSize FROM
    M_Config
WHERE
    `Name` = 'File_Generation_BatchSize';
SELECT 
    CONCAT_WS('',
            'File Generation Batch Size  : ',
            IFNULL(@File_Generation_BatchSize,
                    'NOT CONFIGURED')) AS '';

SELECT RUN_MODE AS '';

IF RUN_MODE<>'CMAB'
THEN 

    SELECT '#############################################################################' AS '';
SELECT 
    CONCAT_WS('',
            'STARTING EVENT EXECUTION FOR FOLLOWING EVENTS .. !! ',
            NOW()) AS '';
SELECT 
    ID,
    ChannelName AS `Channel`,
    CLMSegmentName AS Segment,
    RankedPickListStory_Id AS rCoreStory,
    Waterfall,
    Execution_Sequence AS ExecSequence,
    Communication_Cooloff AS Cooloff,
    Event_Limit AS `Limit`,
    Timeslot_Name AS `TimeSlot`,
    Updated_On AS `Updated`,
    Name
FROM
    Event_Master
WHERE
    state = 'Enabled';
SELECT '############################################################################' AS '';  

END IF;

IF RUN_MODE='CMAB'
THEN 

    SELECT '#############################################################################' AS '';
SELECT 
    CONCAT_WS('',
            'GENERATINNG ELIGIBILITY FILES FOR FOLLOWING EVENTS .. !! ',
            NOW()) AS '';
SELECT 
    ID, Updated_On AS `Updated`, Name
FROM
    Event_Master
WHERE
    state = 'Enabled'
        AND Follows_Waterfall = 100;
SELECT '############################################################################' AS '';  

END IF;


SET done1 = FALSE;
OPEN cur1;
LOOP_ALL_EVENTS: LOOP
SET @CustomerCntFlag = 1;
SET iPrintCreative=0;
	FETCH cur1 into 
	  Event_ID_cur1_1, 
	  Event_Condition_SVOC_1,  
	  Event_Condition_Bill_1, 
	  Reminder_Parent_TriggerId_cur1_1,
	  Reminder_Days_cur1_1,
	  Communication_Template_ID_cur1_1, 
	  V_Offer_Code_1,
	  vEvent_Limit_1,
      vCommunication_CoolOff,
	  vGoodTime_1,
      vCommunication_Channel_1,
	  vRecommendation_Filter_Logic,
      vTimeSlot_Id,
      vCLMSegment_Id;
     
	 SET @Event_ID_cur1=Event_ID_cur1_1;
	 SET @Event_Condition_SVOC=Event_Condition_SVOC_1;
	 SET @Event_Condition_Bill=Event_Condition_Bill_1;
	 SET @Reminder_Parent_TriggerId_cur1=Reminder_Parent_TriggerId_cur1_1;
	 SET @Reminder_Days_cur1=Reminder_Days_cur1_1;
	 SET @Communication_Template_ID_cur1=Communication_Template_ID_cur1_1;
	 SET @V_Offer_Code=V_Offer_Code_1;
	 SET @vEvent_Limit=vEvent_Limit_1;
     SET @vCommunication_CoolOff=vCommunication_CoolOff;
	 SET @vGoodTime=vGoodTime_1;
     SET @vCommunication_Channel=vCommunication_Channel_1;
     SET @vTimeSlot_Id = vTimeSlot_Id;
     SET @vCLMSegment_Id = vCLMSegment_Id;
     
     
     /**** RECREATING ALL VIEWS *******/
	CALL S_CLM_Create_Customer_View(@Event_ID_cur1);

	IF done1 THEN
	   LEAVE LOOP_ALL_EVENTS;
	END IF;
	SET @vStart_Cnt = 0;set @vStart_CntCust=0; set @vEnd_Cnt=0; set @vEnd_CntCust=0;
    
    
	SELECT 
    IFNULL(Follows_Waterfall, - 1)
INTO @event_waterfall FROM
    Event_Master
WHERE
    ID = @Event_ID_cur1;
    
    
	 
	SELECT 
    COUNT(1)
INTO @Customer_Count FROM
    CDM_Customer_Master;
	IF @vEvent_Limit > 0 AND @vEvent_Limit <= 1
	THEN
		SET @vEvent_Limit = round(@Customer_Count*@vEvent_Limit);
	ELSEIF @vEvent_Limit > 1 
	THEN
		SET @DONOTHING = 1; 
	ELSE
		SET @vEvent_Limit = 10000000;
	END IF;
     
    
	IF @Reminder_Parent_TriggerId_cur1>0
	THEN
		SET @global_cooloff_condition = " 1 = 1 ";
		SELECT 
    ID
INTO @Reminder_Parent_EvntId FROM
    Event_Master
WHERE
    `Name` IN (SELECT 
            CampTriggerName
        FROM
            CLM_Campaign_Trigger
        WHERE
            CampTriggerId = @Reminder_Parent_TriggerId_cur1);
			SET @reminder_condition = CONCAT_WS('',"Customer_Id in (select Customer_Id from Event_Execution_History where Event_Id= ",@Reminder_Parent_EvntId," and 
				  F_Get_Std_Date(Event_Execution_Date_ID) = DATE_SUB(curdate(), interval ",@Reminder_Days_cur1," day))"); 
	ELSE
		SET @reminder_condition = " 1 = 1 ";
	END IF;


	
	IF @Reco_Strategy = 'Stories'
	THEN
	   SELECT RankedPickListStory_Id into @RankedPickListStory_Id from Event_Master where ID= @Event_ID_cur1;
	   
	ELSE
		SELECT Number_Of_Recommendation into @Number_Of_Recommendation from Event_Master where ID= @Event_ID_cur1;
        
	END IF;
 

 
	
 	IF RUN_MODE='TEST'  
	THEN 

		SET @vEvent_Limit = "10";
		SET @Overall_Limit = "100";
        SET @event_cooloff_condition="  1 = 1 ";
        SET @global_cooloff_condition=" 1 = 1 ";
        SET @Waterfall_Condition=' 1=1 ';

	END IF;


    IF RUN_MODE='CMAB'  
	THEN 
    
		SET @vEvent_Limit = "1000000000";
		SET @Overall_Limit = "1000000000";
        SET @File_Generation_Type = "Event";

	END IF;


	
	IF @Event_Condition_SVOC IS NULL OR @Event_Condition_SVOC = ''
	THEN
		SET @Event_Condition_SVOC = ' 1 = 1 ';
	END IF;


SET @LastSelectedcnt=0;
SET @LastRejectedCnt=0;
PROCESS_EVENT_EXECUTION_SVOC_QUERY: LOOP
	
	SET @tstart=now();
	SET @vEnd_CntCust = @vStart_CntCust + @vBatchSizeCust;
	IF  @Event_Condition_Bill IS NULL OR @Event_Condition_Bill = ''
	THEN
		SET @SVOT_Condition='SELECT 1 from DUAL';
	ELSE
		SET @SVOT_Condition = CONCAT_WS('','SELECT 1 from V_CLM_Bill_Detail_One_View svot 
		WHERE  svoc.Customer_id=svot.Customer_Id  
		AND  Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'
		AND ',@Event_Condition_Bill,'');
	END IF;

	
	IF @RUN_MODE <> 'TEST' AND @RUN_MODE <> 'Eligiblity' and v_holdout_flag <> 'HOLDOUT_DONE'
	THEN
		SELECT `Value` into @New_Hold_Out_Flag FROM M_Config where `Name`='New_Control_Group_Mechanism';
		IF IFNULL(@New_Hold_Out_Flag,'N')='Y' THEN
			 CALL S_CLM_Hold_Out_New();
		ELSE 
			 CALL S_CLM_Hold_Out(@vStart_CntCust,@vEnd_CntCust);
			
SELECT 
    `Value`
INTO @Hold_Out_Channel_Check FROM
    M_Config
WHERE
    `Name` = 'Channel_Level_Hold_Out_Percentage';
             
             IF @Hold_Out_Channel_Check <> -1
             THEN 
				CALL S_CLM_Hold_Out_Channel(@vStart_CntCust,@vEnd_CntCust);
             END IF;
             
		END IF;
	 END IF;

    
     	IF @Commu_CoolOff_Days IS NULL OR @Commu_CoolOff_Days = '' OR @Commu_CoolOff_Days=0 OR @Commu_CoolOff_Days=-1
	THEN
		SET @Global_CoolOff_Condition="  1 = 1 ";
	ELSE
		SET @Global_CoolOff_date_id = replace(date_sub(current_date, interval @Commu_CoolOff_Days day),'-','');
		SET @Global_CoolOff_Condition = CONCAT_WS(''," NOT EXISTS (select 1 from Event_Execution_History eeh
			where eeh.Customer_Id between ",@vStart_CntCust," and ",@vEnd_CntCust," and eeh.Event_Execution_Date_ID >= ",@Global_CoolOff_date_id," and svoc.Customer_Id=eeh.Customer_Id ) ");
	END IF;
 
 
 IF @vCommunication_CoolOff IS NULL OR @vCommunication_CoolOff = '' OR @vCommunication_CoolOff=0 OR @vCommunication_CoolOff=-1
	THEN
		SET @Event_CoolOff_Condition="  1 = 1 ";
	ELSE
		SET @Event_CoolOff_date_id = replace(date_sub(current_date, interval @vCommunication_CoolOff day),'-','');
		SET @Event_CoolOff_Condition = CONCAT_WS(''," NOT EXISTS (select 1 from Event_Execution_History eeh
			where eeh.Customer_Id between ",@vStart_CntCust," and ",@vEnd_CntCust," and eeh.Communication_Template_ID = ",@Communication_Template_ID_cur1," and eeh.Event_Execution_Date_ID >=  ",@Event_CoolOff_date_id," and svoc.Customer_Id=eeh.Customer_Id ) ");
	END IF;
     
     
     IF @vCommunication_Channel = 'SMS' AND @SMS_Commu_CoolOff_Days 
     then 
		SET @Channel_CoolOff_Days=@SMS_Commu_CoolOff_Days;
     END IF;
     IF (@vCommunication_Channel = 'WhatsApp' OR @vCommunication_Channel = 'WHATSAPP'  )
     THEN 
		SET @Channel_CoolOff_Days=@WhatsApp_Commu_CoolOff_Days; 
     END IF;
     IF @vCommunication_Channel = 'PN'
	 THEN 
		SET @Channel_CoolOff_Days=@PN_Commu_CoolOff_Days; 
     END IF;    
     IF (@vCommunication_Channel = 'EMAIL' OR @vCommunication_Channel = 'Email')
     THEN 
		SET @Channel_CoolOff_Days=@Email_Commu_CoolOff_Days; 
     END IF;    
     
	IF @Channel_CoolOff_Days IS NULL OR @Channel_CoolOff_Days = '' OR @Channel_CoolOff_Days =0 OR @Channel_CoolOff_Days=-1
	THEN
		SET @Channel_CoolOff_condition="  1 = 1 ";
	ELSE
		SET @Channel_CoolOff_date_id = replace(date_sub(current_date, interval @Channel_CoolOff_Days day),'-','');
		SET @Channel_CoolOff_Condition = CONCAT_WS(''," NOT EXISTS (select 1 from Event_Execution_History eeh
			where eeh.Customer_Id between ",@vStart_CntCust," and ",@vEnd_CntCust," and eeh.Communication_Channel = '",@vCommunication_Channel,"' and eeh.Event_Execution_Date_ID >=  ",@Channel_CoolOff_date_id," and svoc.Customer_Id=eeh.Customer_Id ) ");
            
	END IF;
    
    
    
    
  /*** Propensity Filter ***/
			SELECT 
    IFNULL(PropensityModel_Id, - 1)
INTO @Propensity_Id FROM
    Event_Master
WHERE
    EventId = @Event_ID_cur1;
		
		IF @Propensity_Id = -1 
        THEN
			SET @Propensity_Condition=" 1 = 1 ";
		END IF;
        
        IF @Propensity_Id <> -1
        THEN
			SELECT CLMSegmentName INTO @SegmentName FROM Event_Master
			WHERE EventId = @Event_ID_cur1;
            
            SET @SegmentName = REPLACE(@SegmentName,')','');
            SET @SegmentName = REPLACE(@SegmentName,'(','');
        
			SELECT 
    ModelName
INTO @PropensityModel FROM
    CLM_MCore_Model_Master
WHERE
    ID = @Propensity_Id;
            
SELECT 
    IFNULL(PropensityTopN_Percentile, 1)
INTO @Propensity_Percentile FROM
    Event_Master
WHERE
    ID = @Event_ID_cur1;
                
			IF @SegmentName = 'ALL'
            THEN 
				SET @PropensityModel=CONCAT(@PropensityModel,'_All');
                
                SET @Propensity_Condition=CONCAT_WS(''," exists (Select 1 from CLM_MCore_Customer_Percentile CM where Customer_Id between ",@vStart_CntCust," and ",@vEnd_CntCust," and ",@PropensityModel,">= (1-",@Propensity_Percentile,") and svoc.Customer_Id=CM.Customer_Id order by ",@PropensityModel," desc)") ;
                
			ELSE
				SET @PropensityModel=CONCAT(@PropensityModel,'_CLMSegment'); 
                
                SET @Propensity_Condition=CONCAT_WS(''," exists (Select 1 from CLM_MCore_Customer_Percentile CM where Customer_Id between ",@vStart_CntCust," and ",@vEnd_CntCust," and CLMSegmentName= '",@SegmentName,"' and ",@PropensityModel,">= (1-",@Propensity_Percentile,") 
 and svoc.Customer_Id=CM.Customer_Id  order by ",@PropensityModel," desc)") ;
                
                
			END IF;

		END IF;
		
        

	SET @Waterfall_Condition = CONCAT_WS(''," not exists (Select 1 from Event_Execution_Current_Run ee ,Event_Master em where ee.Customer_Id between ",@vStart_CntCust," and ",@vEnd_CntCust," and ee.Event_Id=em.ID and ee.Customer_Id=svoc.Customer_Id and em.Follows_Waterfall not in (99) and ee.Event_Execution_Date_ID=",@Event_Execution_Date_ID," and em.Timeslot_Id=",@vTimeSlot_Id," and  ee.Communication_Channel=",'@vCommunication_Channel',") ");

  
     IF @event_waterfall=WATERFALL_COMPLIANCE
    THEN
        SET @Global_CoolOff_Condition=" 1 = 1 ";
        SET @Event_CoolOff_Condition=" 1 = 1 ";
        SET @Channel_CoolOff_Condition=" 1 = 1 ";
        SET @Waterfall_Condition=" 1 = 1 ";
    END IF;
    
  
	IF @Reminder_Parent_TriggerId_cur1>0
	THEN
		SET @Global_CoolOff_Condition=" 1 = 1 ";
        SET @Event_CoolOff_Condition=" 1 = 1 ";
        SET @Channel_CoolOff_Condition=" 1 = 1 ";
		SET @Waterfall_Condition=" 1 = 1 ";
        SET @Propensity_Condition=" 1 = 1 ";
	END IF;


    
    IF @event_waterfall=WATERFALL_CMAB_PERFORMANCE_BASED
    THEN

		SELECT IFNULL(Value,'Y')
				INTO @Waterfall_Flag
				FROM M_Config
				WHERE Name = 'Waterfall_In_CMAB_Events';

				
				IF @Waterfall_Flag = 'N'
				THEN 
					SET @Waterfall_Condition=" 1 = 1 ";
				END IF;
        
    END IF;
   
   IF RUN_MODE = 'CMAB'
   THEN 
		SET @Waterfall_Condition=' 1=1 ';
		
   END IF;

	INSERT IGNORE INTO Event_Execution_Current_Run
		(`Event_Execution_ID`,
		`Event_ID`,
		`Event_Execution_Date_ID`,
		`Customer_Id`,
		`Is_Target`,
		`Exclusion_Filter_ID`,
		`Communication_Template_ID`,
		`Communication_Channel`,
		`Message`,
        `PN_Message`,
		`In_Control_Group`,
		`In_Event_Control_Group`,
		`LT_Control_Covers_Response_Days`,
		`ST_Control_Covers_Response_Days`,
		`Offer_Code`,
		`Campaign_Key`,
		`Segment_Id`,
		`Revenue_Center`,
		`Recommendation_Product_Id_1`,
		`Recommendation_Product_Id_2`,
		`Recommendation_Product_Id_3`,
		`Recommendation_Product_Id_4`,
		`Recommendation_Product_Id_5`,
		`Recommendation_Product_Id_6`,
		`RUN_MODE`,
		`Microsite_URL`)
		SELECT
		Event_Execution_ID,
		Event_ID,
		Event_Execution_Date_ID,
		Customer_Id,
		Is_Target,
		Exclusion_Filter_ID,
		Communication_Template_ID,
		Communication_Channel,
		Message,
        PN_Message,
		In_Control_Group,
		In_Event_Control_Group,
		LT_Control_Covers_Response_Days,
		ST_Control_Covers_Response_Days,
		Offer_Code,
		Campaign_Key,
		Segment_Id,
		Revenue_Center,
		Recommendation_Product_Id_1,
		Recommendation_Product_Id_2,
		Recommendation_Product_Id_3,
		Recommendation_Product_Id_4,
		Recommendation_Product_Id_5,
		Recommendation_Product_Id_6,
		RUN_MODE,
		Microsite_URL
		FROM Event_Execution;
    
    SET @eventcnt=CONCAT_WS('',"select count(1) into @outcount1 from Event_Execution_Current_Run where In_Control_Group is NULL AND IN_Event_Control_Group IS NULL AND Event_id = ",@Event_ID_cur1,";");
			PREPARE statement2 from @eventcnt;
			Execute statement2;
			Deallocate PREPARE statement2;
    IF @outcount1 >= @vEvent_Limit
			THEN 
				SELECT CONCAT_WS('',"EVENT LIMIT REACHED. NOT PROCESSING MORE RECORDS : ",@outcount1);
				LEAVE PROCESS_EVENT_EXECUTION_SVOC_QUERY;
			END IF;

			SELECT 
    COUNT(1)
INTO @outcount2 FROM
    Event_Execution_Current_Run
WHERE
    In_Control_Group IS NULL
        AND IN_Event_Control_Group IS NULL;
            IF @outcount2 >= @Overall_Limit
			then 
				SELECT CONCAT_WS('',"GLOBAL LIMIT REACHED. NOT PROCESSING MORE RECORDS : ",@outcount2);
				LEAVE PROCESS_EVENT_EXECUTION_SVOC_QUERY;
			END IF;
            
	TRUNCATE Event_Execution;
            


			IF @event_waterfall=WATERFALL_CMAB_PERFORMANCE_BASED AND RUN_MODE <> 'CMAB'
			THEN 

				SET @SQL1= CONCAT('INSERT IGNORE INTO Event_Execution 
				(
						Customer_Id,
						Segment_Id,
						Event_Id,
						Communication_Template_ID,
						Offer_Code,
						Event_Execution_Date_ID,
						Event_Execution_ID,
						Is_Target,
						Communication_Channel
					)
					SELECT 
						Customer_id,'
						,@vCLMSegment_Id,','
						,@Event_ID_cur1,','
						,@Communication_Template_ID_cur1,', "'
						,@V_Offer_Code,'", '
						,@Event_Execution_Date_ID,','
						,'CONCAT(Customer_Id,"-",',@Event_Execution_Date_ID,',"-",',@Event_ID_cur1,',"-","',@vCommunication_Channel,'","-",',@vTimeSlot_Id,'), 
						"Y","'
						,@vCommunication_Channel,'"','
					FROM V_CLM_CMAB_View svoc 
					WHERE Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust
					,' AND Event_Id=',@Event_ID_cur1
					,' AND ',@Waterfall_Condition
					,' Order By Priority limit ',@vEvent_Limit);

			ELSE

			SET @SQL1= CONCAT('INSERT IGNORE INTO Event_Execution 
               (
					Customer_Id,
                    Segment_Id,
					Event_Id,
					Communication_Template_ID,
					Offer_Code,
					Event_Execution_Date_ID,
					Event_Execution_ID,
					Is_Target,
					Communication_Channel
                )
				SELECT 
					Customer_id,'
                    ,@vCLMSegment_Id,','
                    ,@Event_ID_cur1,','
					,@Communication_Template_ID_cur1,', "'
					,@V_Offer_Code,'", '
					,@Event_Execution_Date_ID,','
					,'CONCAT(Customer_Id,"-",',@Event_Execution_Date_ID,',"-",',@Event_ID_cur1,',"-","',@vCommunication_Channel,'","-",',@vTimeSlot_Id,'), 
                    "Y","'
					,@vCommunication_Channel,'"','
				FROM V_CLM_Customer_One_View svoc 
				WHERE Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust
				,' AND ',@Event_Condition_SVOC
				,' AND ',@reminder_condition
				,' AND ',@Global_CoolOff_Condition
                ,' AND ',@Event_CoolOff_Condition
                ,' AND ',@Channel_CoolOff_Condition
				,' AND ',@Waterfall_Condition
                ,' AND ',@Propensity_Condition
				,' AND EXISTS 
				( ',@SVOT_Condition,' ) 
				 limit ',@vEvent_Limit);
                 
				
			END IF;
	
                             
			IF @vStart_CntCust <= 0
            THEN
				SET @SQLx=CONCAT_WS('','SELECT Waterfall into @waterfallcategory from Event_Master where ID=',@Event_ID_cur1,';');
                PREPARE stmt1 from @SQLx; EXECUTE stmt1;DEALLOCATE PREPARE stmt1;
				SELECT 
    CONCAT_WS('',
            'Event Started: ',
            @Event_ID_cur1,
            ' CustomerId: ',
            @vStart_CntCust,
            '-',
            @vEnd_CntCust,
            ' Time: ',
            NOW(),
            ' Limit: ',
            @vEvent_Limit,
            ' Cooloff: ',
            @vCommunication_CoolOff,
            ' WaterfallCategory: ',
            @waterfallcategory,
            ' rCoreStory: ',
            @RankedPickListStory_Id,
            ' <SQL> ',
            @SQL1,
            ' <SQL> ') AS '';
				SELECT 
    COUNT(1)
INTO cov1 FROM
    V_CLM_Customer_One_View
WHERE
    Customer_Id BETWEEN @vStart_CntCust AND @vEnd_CntCust;
						SELECT 
    COUNT(DISTINCT Customer_Id)
INTO cov2 FROM
    Customer_One_View
WHERE
    Customer_Id BETWEEN @vStart_CntCust AND @vEnd_CntCust;

						IF cov1>cov2
						THEN
						  SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'ERROR : There are duplicates in V_CLM_Customer_One_View. This will cause same customer getting generated twice';
						END IF;
                    
            END IF;		
            
            
            SELECT @SQL1 as 'SQL_Condition';
            
            
            PREPARE stmt1 from @SQL1;
            EXECUTE stmt1;
            DEALLOCATE PREPARE stmt1;
            
          	SET @tinserted=now();
            
    		SET @sqlcnt =CONCAT_WS('','SELECT COUNT(1) INTO @CustomerCountInThisBatch FROM Event_Execution where Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,' and Event_Id = ',@Event_ID_cur1,';');
            
            
            
            
            PREPARE stmt1 from @sqlcnt; 
            EXECUTE stmt1; 
            DEALLOCATE PREPARE stmt1;

			IF RUN_MODE = 'CMAB'
			THEN 
				SET @CustomerCountInThisBatch = 1;
			END IF;
            
			If @CustomerCountInThisBatch > 0
            THEN 

				CALL S_CLM_Exclusion_Filter_Execution(@Event_ID_cur1,@vCommunication_Channel,@vStart_CntCust,@vEnd_CntCust); 
				SET @tfilter=now();
				
				IF RUN_MODE <> 'CMAB'
				THEN 

					IF @Reco_Strategy = 'Stories' AND @RankedPickListStory_Id >=0
					THEN
							
							IF @vStart_CntCust <= 0 
								THEN Select CONCAT_WS('','Recos Using NEW rCORE STORY METHOD!! EventId: ',@Event_ID_cur1,' Template: ', @Communication_Template_ID_cur1 ) as ''; 
							END IF;
							
									SELECT 
    IFNULL(`Value`, 'RankedPickList_Stories')
INTO @Rcore_Table FROM
    M_Config
WHERE
    `Name` = 'Rcore_Results_Table_Name'; 
											
									SET @recos=CONCAT('update Event_Execution ees,',@Rcore_Table,' rps
									set ees.PN_Message = rps.Recos, ees.Is_Target="X",ees.Recommendation_Product_Id_1 = rps.Reco_Product_1,ees.Recommendation_Product_Id_2 = rps.Reco_Product_2,
									ees.Recommendation_Product_Id_3 = rps.Reco_Product_3,
									ees.NumRecos=rps.NumRecos,
									ees.NumCust=rps.NumCust,
									ees.RecoPField1=rps.RecoPField1,
									ees.RecoPField2=rps.RecoPField2,
									ees.Recommendation_Product_Id_3 = rps.Reco_Product_3,
									ees.Recommendation_Product_Id_4 = rps.Reco_Product_4,
									ees.Recommendation_Product_Id_5 = rps.Reco_Product_5,
									ees.Recommendation_Product_Id_6 = rps.Reco_Product_6
									where ees.Customer_Id = rps.Customer_Id 
									and rps.Story_Id= ',@RankedPickListStory_Id,'
									and  ees.Event_id = ',@Event_ID_cur1,'
									and ees.Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,'');
									PREPARE stmt1 from @recos;
									EXECUTE stmt1;
									DEALLOCATE PREPARE stmt1;
							
							
									UPDATE Event_Execution ee,
    Event_Master em 
SET 
    ee.Is_Target = 'B',
    ee.Message = CONCAT_WS('',
            'Recommendation Not Available ',
            ' rCore strategy is :',
            @Reco_Strategy,
            ' rCore Story Id : ',
            @RankedPickListStory_Id)
WHERE
    ee.Is_Target <> 'X'
        AND ee.Event_id = @Event_ID_cur1
        AND em.ID = ee.Event_id
        AND em.RankedPickListStory_Id > 0
        AND em.RankedPickListStory_Id = @RankedPickListStory_Id
        AND ee.Customer_Id BETWEEN @vStart_CntCust AND @vEnd_CntCust;
														
											INSERT IGNORE into Event_Execution_Rejected 
														SELECT * from Event_Execution WHERE Is_Target='B' AND Customer_Id between @vStart_CntCust and @vEnd_CntCust;
														
									DELETE FROM Event_Execution 
WHERE
    Is_Target = 'B'
    AND Customer_Id BETWEEN @vStart_CntCust AND @vEnd_CntCust;
														
									UPDATE Event_Execution ee 
SET 
    ee.Is_Target = 'Y'
WHERE
    ee.Is_Target = 'X'
        AND ee.Event_id = @Event_ID_cur1
        AND ee.Customer_Id BETWEEN @vStart_CntCust AND @vEnd_CntCust;
									
						ELSEIF  @Reco_Strategy <> 'Stories' and ifnull(@Number_Of_Recommendation,0) > 0
						THEN
							IF @vStart_CntCust <= 0 THEN Select CONCAT_WS('','Recos Using OLD rCORE ALGO METHOD!!!! EventId: ',@Event_ID_cur1,' Template: ',@Communication_Template_ID_cur1) as ''; 
							END IF;			 
							CALL S_CLM_Event_Execution_Recommendation(@Event_ID_cur1,@Communication_Template_ID_cur1,@vStart_CntCust,@vEnd_CntCust);                    
							INSERT IGNORE into Event_Execution_Rejected 
							SELECT * from Event_Execution WHERE (Is_Target='A' OR Is_Target='B') AND Customer_Id between @vStart_CntCust and @vEnd_CntCust;
							DELETE FROM Event_Execution 
WHERE
    (Is_Target = 'A' OR Is_Target = 'B')
    AND Customer_Id BETWEEN @vStart_CntCust AND @vEnd_CntCust;
												
												UPDATE Event_Execution ee 
SET 
    ee.Is_Target = 'Y'
WHERE
    ee.Is_Target = 'X'
        AND ee.Event_id = @Event_ID_cur1
        AND ee.Customer_Id BETWEEN @vStart_CntCust AND @vEnd_CntCust;

						ELSE
							set @donothing=1;
							IF @vStart_CntCust <= 0 
							THEN 
								Select CONCAT_WS('','Event is non recommendation based!! EventId: ',@Event_ID_cur1,' Template: ',@Communication_Template_ID_cur1) as ''; 			  
							END IF;
						END IF;
					END IF;


               SET @trecommendation=now();
				SELECT '' AS '';

				SELECT 
    IFNULL(Needs_ControlGroup, 'Y')
INTO @ControlGroup_Flag FROM
    Event_Master
WHERE
    EventId = @Event_ID_cur1;

				
				IF @ControlGroup_Flag = 'Y'
				THEN 
					CALL S_CLM_Event_Execution_ControlGroup(@Event_ID_cur1,@Event_Execution_Date_ID,@vStart_CntCust,@vEnd_CntCust);
				END IF;
                
                CALL S_CLM_PField_Check(@Communication_Template_ID_cur1,@vStart_CntCust,@vEnd_CntCust);         
                
				SET @tfilegeneratationstart=now(); 
                SET @sqlcnt=concat('SELECT COUNT(1) INTO @v_CntCustomerReadyForFile FROM Event_Execution where In_Control_Group is NULL and In_Event_Control_Group is NULL and Customer_Id between ',@vStart_CntCust,' and ',@vEnd_CntCust,' and Event_Id = ',@Event_ID_cur1,';');
				PREPARE stmt1 from @sqlcnt; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;
				IF @v_CntCustomerReadyForFile > 0 AND @File_Generation_Type = 'Customer_Batch' and @pfieild_proc = 1 
				THEN
                
                     IF RUN_MODE <> 'CMAB' AND RUN_MODE <> 'TEST'
					 THEN 
                        
                        /* Insert THe Record If Not Available */
                        INSERT IGNORE INTO Event_Execution_History_Summary(Event_Id,Event_Execution_Date_Id,Start_Time,UI_Status)
                        SELECT @Event_ID_cur1,@Event_Execution_Date_ID,now(),'Started';
                        
					
					/* Update the End Time Of The Event */
					
					UPDATE Event_Execution_History_Summary
					SET UI_Status = 'Running'
					WHERE Event_Id = @Event_ID_cur1
					AND  Event_Execution_Date_Id = @Event_Execution_Date_ID;
				  
				   END IF;
						
                
                
					CALL S_CLM_Communication_File_Gen_CustLoop(iPrintCreative,@Event_ID_cur1,@Communication_Template_ID_cur1,RUN_MODE,@vStart_CntCust,@vEnd_CntCust,@vEvent_Limit,@Overall_Limit,@Out_FileName,@Out_FileHeader,@Out_FileLocation);
                    
                    
				END IF;
                SET @tfilegeneratationend=now();
 			
             
             IF RUN_MODE <> 'CMAB' AND RUN_MODE <> 'TEST'
			 THEN 
             
             
             /*Update the Progress Of the Event*/
             UPDATE Event_Execution_History_Summary 
             SET Progress = (CASE 
                            WHEN round((@vEnd_CntCust/@Rec_Cnt)*100) > 100 THEN 100
                            ELSE round((@vEnd_CntCust/@Rec_Cnt)*100)
                            END)
			WHERE Event_Id = @Event_ID_cur1
		    AND  Event_Execution_Date_Id = @Event_Execution_Date_ID;
            
           
          
           END IF;
            
            
            
        		SELECT 
    COUNT(1)
INTO @Rejected_Total FROM
    Event_Execution_Rejected
WHERE
    Event_Id = @Event_ID_cur1;
					
				SELECT 
    COUNT(1)
INTO @Selected_Total FROM
    Event_Execution_Current_Run
WHERE
    Event_Id = @Event_ID_cur1;
                    
	
			END IF;
									
		
			SET @LastSelectedcnt=@Selected_Total;
			SET @LastRejectedCnt=@Rejected_Total;
		
			SET @eventcnt=concat("select count(1) into @outcount1 from Event_Execution_Current_Run where Event_id = ",@Event_ID_cur1,";");
			PREPARE statement2 from @eventcnt;
			Execute statement2;
			Deallocate PREPARE statement2;
            set @sqllog=
					CONCAT_WS('','Event Id: ',
							ifnull(@Event_ID_cur1,'NA'),
                            ' CustomerId: ', ifnull(@vStart_CntCust,0),'-',ifnull(@vEnd_CntCust,0),
                            ' #Selected: ',ifnull(@CustomerCountInThisBatch,0),
							' #SelectedAfterReco: ',ifnull(@v_CntCustomerReadyForFile,0),
                            ' #SelectedAfterMsg: ',ifnull(@outcount1,0),
							' Batch Selected: ',ifnull(@Selected_Total,0)-ifnull(@LastSelectedcnt,0),
							' Batch Rejected: ',ifnull(@Rejected_Total,0)-ifnull(@LastRejectedCnt,0),
                            ' Time(Selection): ',if(@tinserted>=@tstart,ifnull(timediff(@tinserted,@tstart),'NA'),0),
                            ' Time(Exclusion): ',if(@tfilter>=@tinserted,ifnull(timediff(@tfilter,@tinserted),'NA'),0),
                            ' Time(Reco): ',if(@trecommendation>=@tfilter,ifnull(timediff(@trecommendation,@tfilter),'NA'),0),
                            ' Time(Other): ',if(@tfilegeneratationstart>=@trecommendation,ifnull(timediff(@tfilegeneratationstart,@trecommendation),'NA'),0),
							' Time(Msg): ',if(@tfilegeneratationend>=@tfilegeneratationstart,ifnull(timediff(@tfilegeneratationend,@tfilegeneratationstart),'NA'),0),
                            ' TotalSelected: ',	ifnull(@Selected_Total,0),
                            ' TotalRejected: ',ifnull(@Rejected_Total,0)
							) ;
                         								

			SELECT @sqllog AS 'log message ';

			insert into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`, Load_Execution_ID) 
				values ('S_CLM_Event_Execution',@sqllog ,now(),'Summary','NA', '097887');

		
        
			IF @vEnd_CntCust >= @Rec_Cnt
			THEN
			  LEAVE PROCESS_EVENT_EXECUTION_SVOC_QUERY;
			END IF;
            

SET @vStart_CntCust = @vEnd_CntCust + 1;
END LOOP PROCESS_EVENT_EXECUTION_SVOC_QUERY;


INSERT IGNORE INTO Event_Execution_Current_Run
		(`Event_Execution_ID`,
		`Event_ID`,
		`Event_Execution_Date_ID`,
		`Customer_Id`,
		`Is_Target`,
		`Exclusion_Filter_ID`,
		`Communication_Template_ID`,
		`Communication_Channel`,
		`Message`,
        `PN_Message`,
		`In_Control_Group`,
		`In_Event_Control_Group`,
		`LT_Control_Covers_Response_Days`,
		`ST_Control_Covers_Response_Days`,
		`Offer_Code`,
		`Campaign_Key`,
		`Segment_Id`,
		`Revenue_Center`,
		`Recommendation_Product_Id_1`,
		`Recommendation_Product_Id_2`,
		`Recommendation_Product_Id_3`,
		`Recommendation_Product_Id_4`,
		`Recommendation_Product_Id_5`,
		`Recommendation_Product_Id_6`,
		`RUN_MODE`,
		`Microsite_URL`,
		`NumRecos`,
		`NumCust`,
		`RecoPField1`,
		`RecoPField2`)
		SELECT
		Event_Execution_ID,
		Event_ID,
		Event_Execution_Date_ID,
		Customer_Id,
		Is_Target,
		Exclusion_Filter_ID,
		Communication_Template_ID,
		Communication_Channel,
		Message,
        PN_Message,
		In_Control_Group,
		In_Event_Control_Group,
		LT_Control_Covers_Response_Days,
		ST_Control_Covers_Response_Days,
		Offer_Code,
		Campaign_Key,
		Segment_Id,
		Revenue_Center,
		Recommendation_Product_Id_1,
		Recommendation_Product_Id_2,
		Recommendation_Product_Id_3,
		Recommendation_Product_Id_4,
		Recommendation_Product_Id_5,
		Recommendation_Product_Id_6,
		RUN_MODE,
		Microsite_URL,
		NumRecos,
		NumCust,
		RecoPField1,
		RecoPField2
		FROM Event_Execution;

SET v_holdout_flag='HOLDOUT_DONE';


SET @sqlcnt=concat('SELECT COUNT(1) INTO @v_CntCustomerReadyForFile1 FROM Event_Execution where Event_Id = ',@Event_ID_cur1,';');
PREPARE stmt1 from @sqlcnt; EXECUTE stmt1; DEALLOCATE PREPARE stmt1;

IF RUN_MODE = 'CMAB'
THEN 
	SET @v_CntCustomerReadyForFile1 = 1;
END IF;


IF @File_Generation_Type <> 'Customer_Batch' AND @v_CntCustomerReadyForFile1 > 0
THEN 
	SELECT ifnull(Customer_id,0) INTO @Rec_Cnt FROM V_CLM_Customer_One_View ORDER BY Customer_id DESC LIMIT 1;  
	CALL S_CLM_Communication_File_Gen_CustLoop(iPrintCreative,@Event_ID_cur1,@Communication_Template_ID_cur1,RUN_MODE,0,@Rec_Cnt,@vEvent_Limit,@Overall_Limit,@Out_FileName,@Out_FileHeader,@Out_FileLocation); 
END if;



IF @Voucher_Strategy = 'CAPILLARY'
THEN
	CALL S_CLM_Event_Execution_VoucherForCapillary(@Event_ID_cur1,@Communication_Template_ID_cur1,@Event_Execution_Date_ID);
END IF;



SELECT 
    COUNT(1)
INTO @outcount2 FROM
    Event_Execution_Current_Run;
if @outcount2 >= @Overall_Limit
then 
	SELECT CONCAT_WS('',"GLOBAL LIMIT REACHED. NOT PROCESSING MORE RECORDS : ",@outcount2);
	LEAVE LOOP_ALL_EVENTS;
end if;

set @LastSelectedcnt=0;
set @LastRejectedCnt=0;


 /* Update the End Time Of The Event */
            
            UPDATE Event_Execution_History_Summary
            SET End_Time = now()
            WHERE Event_Id = @Event_ID_cur1
		    AND  Event_Execution_Date_Id = @Event_Execution_Date_ID;
            
            
            /* Calculate Duration Of The Event */
            
            UPDATE Event_Execution_History_Summary
            SET Duration = TIMEDIFF(End_Time,Start_Time),
				UI_Status = 'Success'
            WHERE Event_Id = @Event_ID_cur1
		    AND  Event_Execution_Date_Id = @Event_Execution_Date_ID;
            

END LOOP LOOP_ALL_EVENTS;	
CLOSE cur1;

SELECT '#############################################################################' AS '';
SELECT 
    CONCAT_WS('',
            ' ALL EVENTS EXECUTION COMPLETED !! ',
            NOW()) AS '';
SELECT 'REJECTED COMMUNICATIONS SUMMARY ' AS '';
SELECT 
    Event_Id,
    COUNT(1),
    Message,
    Email_Message AS `Failure Reason`
FROM
    Event_Execution_Rejected
GROUP BY Event_Id , Message , Email_Message;
SELECT 'SUCCESSFUL COMMUNICATIONS SUMMARY ' AS '';
SELECT 
    Event_Id, COUNT(1)
FROM
    Event_Execution_Current_Run
GROUP BY Event_Id;
SELECT '############################################################################' AS '';


SELECT 
    COUNT(1)
INTO @vCustomer_Count FROM
    Event_Execution_Current_Run;
If  @vCustomer_Count =0 
Then
    SET @vSuccess = 1;
	SET @vErrMsg = CONCAT_WS('','\r\n','Event Created but not tested');
ELSE
	SET @vSuccess = 1;
	SET @vErrMsg = '';
End if;
SELECT @vCustomer_Count INTO no_of_customers;
SELECT @vSuccess INTO vSuccess;
SELECT @vErrMsg INTO vErrMsg;

UPDATE ETL_Execution_Details 
SET 
    Status = 'Succeeded',
    End_Time = NOW()
WHERE
    Procedure_Name = 'S_CLM_Event_Execution'
        AND Execution_ID = Exe_ID;
SELECT 
    'S_CLM_Event_Execution : COMPLETED ' AS '',
    NOW() AS '',
    'No of Customers : ' AS '',
    @vCustomer_Count AS '',
    ' Message :' AS '',
    @vErrMsg AS '';
    
Insert into ETL_Execution_Details(Procedure_Name,job_name,Start_Time,End_Time,Load_Execution_ID)	
Select "S_CLM_Event_Execution",Concat("Reason",Rejection_Reson,": Counts of Customer ",Counts), NOW(), NOW(),Exe_ID
From (
SELECT  Distinct Concat(Message) as Rejection_Reson,Count(1) as Counts
FROM Event_Execution_Rejected Group by Message,Event_id	)A;

END

