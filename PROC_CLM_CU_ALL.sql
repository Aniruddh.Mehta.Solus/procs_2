CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_Change_Event_Execution_Sequence`(
	IN vEventId BIGINT,
	IN vExecution_Sequence BIGINT
)
Begin


SET SQL_SAFE_UPDATES=0;


Update CLM_Campaign_Events
set Execution_Sequence = vExecution_Sequence
where EventId=vEventId ;

Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set B.Execution_Sequence = vExecution_Sequence
where A.CampTriggerId=B.CampTriggerId and EventId=vEventId ;


END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_Change_Event_Status`(
	IN vEventId BIGINT,
	IN vCTState BIGINT
)
Begin


SET SQL_SAFE_UPDATES=0;

if vCTState=0 then
Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set CTState = 0
where EventId=vEventId and A.CampTriggerId =B.CampTriggerId;

end if;

if vCTState=1 then
Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set CTState = 1
where EventId=vEventId and A.CampTriggerId =B.CampTriggerId;
end if;

if vCTState=2 then
Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set CTState = 2
where EventId=vEventId and A.CampTriggerId =B.CampTriggerId;

end if;



END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Campaign`(
	IN vUserId BIGINT, 
	IN vCampaignName VARCHAR(128), 
	IN vCampaignDesc VARCHAR(1024), 
	IN vCampaignThemeId BIGINT, 
	IN vCLMSegmentId BIGINT,
	IN vCampaignCGPercentage DECIMAL(5,2),
	IN vCampaignResponseDays BIGINT,
	IN vCampaignState TINYINT,
	IN vCampaignSmartPtyState TINYINT,
	IN vCampaignIsOnDemand TINYINT,
	IN vCampaignUsesWaterfall TINYINT,
	IN vCampaignIsPerpetual TINYINT,
	IN vCampaignStartDate DATE,
	IN vCampaignEndDate DATE,
	IN vExecution_Sequence BIGINT,
	INOUT vCampaignId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Campaign';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCampaignName IS NULL OR vCampaignName = '' THEN
		SET vErrMsg = 'Campaign Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vCampaignId IS NULL OR vCampaignId <= 0 THEN
			SET vSqlstmt = CONCAT(
				' INSERT IGNORE INTO CLM_Campaign (
					CampaignName, 
					CampaignDesc, 
					CampaignThemeId, 
					CLMSegmentId,
					CampaignCGPercentage,
					CampaignResponseDays,
					CampaignState,
					CampaignSmartPtyState,
					CampaignIsOnDemand,
					CampaignUsesWaterfall,
					CampaignIsPerpetual,
					CampaignStartDate,
					CampaignEndDate,
					Execution_Sequence,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				,'"', vCampaignName,'"'
				,', "', vCampaignDesc,'"'
				,', ', vCampaignThemeId 
				,', ', vCLMSegmentId 			
				,', ', vCampaignCGPercentage
				,', ', vCampaignResponseDays
				,', ', vCampaignState
				,', ', vCampaignSmartPtyState
				,', ', vCampaignIsOnDemand
				,', ', vCampaignUsesWaterfall
				,', ', vCampaignIsPerpetual
				,', "',vCampaignStartDate,'"'
				,', "',vCampaignEndDate, '"'
				,', ', vExecution_Sequence
				,', ', vUserId
				,', ',vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;

			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
				
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events 
				WHERE 
					CampaignId = vCampaignId
					AND Has_Records_In_Execution_Hist =  1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					' INSERT IGNORE INTO CLM_Campaign (
						CampaignId,
						CampaignName, 
						CampaignDesc, 
						CampaignThemeId, 
						CLMSegmentId,
						CampaignCGPercentage,
						CampaignResponseDays,
						CampaignState,
						CampaignSmartPtyState,
						CampaignIsOnDemand,
						CampaignUsesWaterfall,
						CampaignIsPerpetual,
						CampaignStartDate,
						CampaignEndDate,
						Execution_Sequence,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vCampaignId
					,', "', vCampaignName,'"'
					,', "', vCampaignDesc,'"'
					,', ', vCampaignThemeId 
					,', ', vCLMSegmentId 			
					,', ', vCampaignCGPercentage
					,', ', vCampaignResponseDays
					,', ', vCampaignState
					,', ', vCampaignSmartPtyState
					,', ', vCampaignIsOnDemand
					,', ', vCampaignUsesWaterfall
					,', ', vCampaignIsPerpetual
					,', "',vCampaignStartDate,'"'
					,', "',vCampaignEndDate, '"'
					,', ', vExecution_Sequence
					,', ', vUserId
					,', ',vUserId
					,') ON DUPLICATE KEY UPDATE '
					, ' CampaignDesc = "', vCampaignDesc, '"'
					, ', CampaignThemeId = ', vCampaignThemeId
					, ', CLMSegmentId = ', vCLMSegmentId
					, ', CampaignCGPercentage = ', vCampaignCGPercentage
					, ', CampaignResponseDays = ', vCampaignResponseDays
					, ', CampaignState = ', vCampaignState
					, ', CampaignSmartPtyState = ', vCampaignSmartPtyState
					, ', CampaignIsOnDemand = ', vCampaignIsOnDemand
					, ', CampaignUsesWaterfall = ', vCampaignUsesWaterfall
					, ', CampaignIsPerpetual = ', vCampaignIsPerpetual
					, ', CampaignStartDate ="', vCampaignStartDate, '"'
					, ', CampaignEndDate ="', vCampaignEndDate, '"'
					, ', Execution_Sequence = ', vExecution_Sequence
					, ', CreatedBy = ', vUserId
					, ', ModifiedBy = ', vUserId
					, ';'
				);

				SET vErrMsg = vSqlstmt;
				
				SET @sql_stmt = vSqlstmt;
                select @sql_stmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Campaign already exists and has been executed and hence cannot be modified.';
				SET vSuccess = 0;
			END IF;
		END IF;
	END IF;
	
	SELECT CampaignId INTO vCampaignId 
	FROM CLM_Campaign
	WHERE 
		CampaignName = vCampaignName
        AND CLMSegmentId = vCLMSegmentId
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Campaign_Events`(
	IN vUserId BIGINT,  
	IN vEventName VARCHAR(1024), 
	IN vExtEventName VARCHAR(1024), 
	IN vCampaignThemeId BIGINT, 
	IN vCampaignId BIGINT,  
	IN vCLMSegmentId BIGINT,  
	IN vCampTriggerId BIGINT,  
	IN vChannelId BIGINT,  
	IN vMicroSegmentId BIGINT,  
	IN vCreativeId BIGINT,  
	IN vExecution_Sequence BIGINT,  
	INOUT vEventId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Campaign_Events';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);
	set @vEventName=vEventName;
	IF vEventName IS NULL OR vEventName = '' THEN
		SET vErrMsg =  concat('Event Name is a Mandatory Field and cannot be empty or NULL ',@vEventName,'');
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vEventId IS NULL OR vEventId <= 0 THEN
			SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Campaign_Events (
						EventName,
						ExtEventName,
						CampaignThemeId,
						CampaignId,
						CLMSegmentId,
						CampTriggerId,
						ChannelId,
						MicroSegmentId,
						CreativeId,
						Execution_Sequence,
						CreatedBy
					) VALUES ('
					,'"', vEventName, '"'				
					,',"', vExtEventName, '"'				
					,',', vCampaignThemeId
					,',', vCampaignId
					,',', vCLMSegmentId
					,',', vCampTriggerId
					,',', vChannelId
					,',', vMicroSegmentId
					,',', vCreativeId
					,',', vExecution_Sequence
					,',', vUserId
					,')'
				);

			SET vErrMsg = vSqlstmt;
            
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
			
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events
				WHERE 
					EventId = vEventId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				
				SET vSqlstmt = CONCAT(
						'UPDATE IGNORE CLM_Campaign_Events
						SET
							EventName = ','"', vEventName, '"'
							, ', ExtEventName = ','"', vExtEventName, '"'
							, ', CampaignThemeId = ', vCampaignThemeId
							, ', CampaignId = ', vCampaignId
							, ', CLMSegmentId = ', vCLMSegmentId
							, ', CampTriggerId = ', vCampTriggerId
							, ', ChannelId = ', vChannelId
							, ', MicroSegmentId = ', vMicroSegmentId
							, ', CreativeId = ', vCreativeId
							, ', Execution_Sequence = ', vExecution_Sequence
							, ', CreatedBy = ', vUserId
						, ' WHERE EventId = ', vEventId
						, ' AND Has_Records_In_Execution_Hist = 0'
					);
				
				SET vErrMsg = vSqlstmt;
				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Campaign Event already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT EventId  INTO vEventId
	FROM CLM_Campaign_Events 
    WHERE
		CampaignThemeId = vCampaignThemeId
		AND CampaignId = vCampaignId
		AND CampTriggerId = vCampTriggerId
		AND ChannelId = vChannelId
		AND MicroSegmentId = vMicroSegmentId
	LIMIT 1;

	UPDATE
		CLM_Campaign_Events AS CCE,
		CLM_Campaign AS CC,
		CLM_Campaign_Trigger AS CCTRIG,
		CLM_CampaignTheme As CCT
	SET
		CCE.Execution_Sequence = ((CCT.Execution_Sequence * 10000000000000) + (CC.Execution_Sequence * 1000000000) + (CCTRIG.Execution_Sequence * 100000) + CCE.EventId)
		
	WHERE
		CC.CampaignId = CCE.CampaignId
		AND CCE.CampTriggerId = CCTRIG.CampTriggerId
		AND CC.CampaignThemeId = CCT.CampaignThemeId
		AND CCE.EventId = vEventId;
		
		Update CLM_Campaign_Events AS CCE,CLM_Campaign_Trigger AS CCTRIG
		set  CCTRIG.Execution_Sequence =CCE.Execution_Sequence
		where CCE.CampTriggerId = CCTRIG.CampTriggerId 
		   and      CCTRIG.CampTriggerId=vCampTriggerId;
		

	SET vSuccess = 1;
	
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);
END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Campaign_Trigger`(
	IN vUserId BIGINT,  
	IN vCampTriggerName VARCHAR(128),  
	IN vCampTriggerDesc VARCHAR(1024),  
	IN vPrecedingTriggerId BIGINT, 
	IN vDaysOffsetFromPrecedingTrigger SMALLINT, 
	IN vCTState TINYINT, 
	IN vCTUsesGoodTimeModel	TINYINT, 
	IN vCTIsMandatory TINYINT, 
	IN vIsAReminder TINYINT, 
	IN vCampaignThemeId BIGINT, 
	IN vCampaignId BIGINT, 
	IN vCTReplacedQueryAsInUI TEXT, 
	IN vCTReplacedQuery TEXT, 
	IN vIsValidSVOCCondition TINYINT, 
	IN vCTReplacedQueryBillAsInUI TEXT, 
	IN vCTReplacedQueryBill TEXT, 
	IN vIsValidBillCondition TINYINT, 
	IN vExecution_Sequence BIGINT, 
	INOUT vCampTriggerId BIGINT,
	IN vEvent_Limit BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Campaign_Trigger';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCampTriggerName IS NULL OR vCampTriggerName = '' THEN
		SET vErrMsg = 'CampTrigger Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE

		
		IF vCampTriggerId IS NULL OR vCampTriggerId <= 0 THEN

			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Campaign_Trigger (
					CampTriggerName,
					CampTriggerDesc,
					PrecedingTriggerId,
					DaysOffsetFromPrecedingTrigger,
					CTState,
					CTUsesGoodTimeModel,
					CTIsMandatory,
					IsAReminder,
					CampaignThemeId,
					CampaignId,
					CTReplacedQueryAsInUI,
					CTReplacedQuery,
					IsValidSVOCCondition,
					CTReplacedQueryBillAsInUI,
					CTReplacedQueryBill,
					IsValidBillCondition,
					Execution_Sequence,
					Event_Limit,
					CreatedBy, 
					ModifiedBy 
				) 
				VALUES ('
				,'"', vCampTriggerName, '"'
				,', "', vCampTriggerDesc, '"'
				,',', vPrecedingTriggerId
				,',', vDaysOffsetFromPrecedingTrigger
				,',', vCTState
				,',', vCTUsesGoodTimeModel
				,',', vCTIsMandatory
				,',', vIsAReminder
				,',', vCampaignThemeId
				,',', vCampaignId
				,', "', vCTReplacedQueryAsInUI, '"'
				,', "', vCTReplacedQuery, '"'
				, ',', vIsValidSVOCCondition
				,', "', vCTReplacedQueryBillAsInUI, '"'
				,', "', vCTReplacedQueryBill, '"'
				, ',', vIsValidBillCondition
				, ',', vExecution_Sequence
				, ',', vEvent_Limit
				, ',', vUserId
				, ',', vUserId
				, ')'
			);
			
			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
		ELSE
			
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events
				WHERE 
					CampTriggerId = vCampTriggerId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Campaign_Trigger (
						CampTriggerId,
						CampTriggerName,
						CampTriggerDesc,
						PrecedingTriggerId,
						DaysOffsetFromPrecedingTrigger,
						CTState,
						CTUsesGoodTimeModel,
						CTIsMandatory,
						IsAReminder,
						CampaignThemeId,
						CampaignId,
						CTReplacedQueryAsInUI,
						CTReplacedQuery,
						IsValidSVOCCondition,
						CTReplacedQueryBillAsInUI,
						CTReplacedQueryBill,
						IsValidBillCondition,
						Execution_Sequence,
						Event_Limit,
						CreatedBy, 
						ModifiedBy 
					) 
					VALUES ('
					, vCampTriggerId
					,', "', vCampTriggerName, '"'
					,', "', vCampTriggerDesc, '"'
					,',', vPrecedingTriggerId
					,',', vDaysOffsetFromPrecedingTrigger
					,',', vCTState
					,',', vCTUsesGoodTimeModel
					,',', vCTIsMandatory
					,',', vIsAReminder
					,',', vCampaignThemeId
					,',', vCampaignId
					,', "', vCTReplacedQueryAsInUI, '"'
					,', "', vCTReplacedQuery, '"'
					, ',', vIsValidSVOCCondition
					,', "', vCTReplacedQueryBillAsInUI, '"'
					,', "', vCTReplacedQueryBill, '"'
					, ',', vIsValidBillCondition
					, ',', vExecution_Sequence
					, ',', vEvent_Limit
					, ',', vUserId
					, ',', vUserId
					,') ON DUPLICATE KEY UPDATE '
					,' CampTriggerDesc = "', vCampTriggerDesc, '"'
					,', PrecedingTriggerId = ', vPrecedingTriggerId
					,', DaysOffsetFromPrecedingTrigger = ', vDaysOffsetFromPrecedingTrigger
					,', CTState = ', vCTState
					,', CTUsesGoodTimeModel = ', vCTUsesGoodTimeModel
					,', CTIsMandatory = ', vCTIsMandatory
					,', IsAReminder = ', vIsAReminder
					,', CampaignThemeId = ', vCampaignThemeId
					,', CampaignId = ', vCampaignId
					,', CTReplacedQueryAsInUI = "', vCTReplacedQueryAsInUI, '"'
					,', CTReplacedQuery = "', vCTReplacedQuery, '"'
					,', IsValidSVOCCondition = ', vIsValidSVOCCondition
					,', CTReplacedQueryBillAsInUI = "', vCTReplacedQueryBillAsInUI, '"'
					,', CTReplacedQueryBill = "', vCTReplacedQueryBill, '"'
					,', IsValidBillCondition = ', vIsValidBillCondition
					,', Execution_Sequence = ', vExecution_Sequence
                    ,', Event_Limit = ', vEvent_Limit
					,', CreatedBy = ', vUserId
					,', ModifiedBy = ', vUserId
				);

				SET vErrMsg = vSqlstmt;
				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Campaign Trigger already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT CampTriggerId INTO vCampTriggerId 
	FROM CLM_Campaign_Trigger
	WHERE 
		CampTriggerName = vCampTriggerName
        AND CampaignId = vCampaignId 
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_CampaignTheme`(
	IN vUserId BIGINT, 
	IN vCampaignThemeName VARCHAR(128), 
	IN vCampaignThemeDesc VARCHAR(1024), 
	IN vExecution_Sequence BIGINT, 
	INOUT vCampaignThemeId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_CampaignTheme';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCampaignThemeName IS NULL OR vCampaignThemeName = '' THEN
		SET vErrMsg = 'Campaign Theme is a Mandatory Field and cannot be empty or NULL';
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
			SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_CampaignTheme (
						CampaignThemeName, 
						CampaignThemeDesc, 
						Execution_Sequence,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, '"', vCampaignThemeName, '"'
					, ', "',vCampaignThemeDesc, '"'
					, ', ', vExecution_Sequence
					, ', ', vUserId
					, ', ', vUserId
					, ')'
				);

			SET vErrMsg = vSqlstmt;

			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
			
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events
				WHERE 
					CampaignThemeId = vCampaignThemeId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_CampaignTheme (
						CampaignThemeId,
						CampaignThemeName, 
						CampaignThemeDesc, 
						Execution_Sequence,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vCampaignThemeId
					, ', "', vCampaignThemeName, '"'
					, ', "',vCampaignThemeDesc, '"'
					, ', ', vExecution_Sequence
					, ', ', vUserId
					, ', ', vUserId
					, ') ON DUPLICATE KEY UPDATE '
					, ' CampaignThemeDesc = "', vCampaignThemeDesc,'"'
					, ', Execution_Sequence = ', vExecution_Sequence
					, ', CreatedBy = ', vUserId
					, ', ModifiedBy = ', vUserId
				);

				SET vErrMsg = vSqlstmt;

				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Campaign Theme already exists and has been used in Campaigns already and hence cannot be modified.';
				SET vSuccess = 0;
				
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
		
			END IF;
		END IF;
     
		SELECT CampaignThemeId INTO vCampaignThemeId 
		FROM CLM_CampaignTheme
		WHERE CampaignThemeName = vCampaignThemeName
		LIMIT 1;
	END IF;
	
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);
END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Channel`(
	IN vUserId BIGINT, 
	IN vChannelName VARCHAR(128), 
	IN vChannelDesc VARCHAR(1024), 
	INOUT vChannelId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Channel';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vChannelName IS NULL OR vChannelName = '' THEN
		SET vErrMsg = 'Channel Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vChannelId IS NULL OR vChannelId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Channel (
					ChannelName, 
					ChannelDesc, 
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				,'"', vChannelName,'"'
				,',"', vChannelDesc,'"'
				,',', vUserId
				,',', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
				
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events 
				WHERE 
					ChannelId = vChannelId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Channel (
						ChannelId,
						ChannelName, 
						ChannelDesc, 
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vChannelId
					,',"', vChannelName,'"'
					,',"', vChannelDesc,'"'
					,',', vUserId
					,',', vUserId
					,') ON DUPLICATE KEY UPDATE '
					,' ChannelDesc = "', vChannelDesc, '"'
					,', CreatedBy = ', vUserId
					,', ModifiedBy = ', vUserId
				);

				SET vErrMsg = vSqlstmt;
				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Channel already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT ChannelId INTO vChannelId 
	FROM CLM_Channel
	WHERE ChannelName = vChannelName
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Creative`(
	IN vUserId BIGINT, 
	IN vCreativeName VARCHAR(128), 
	IN vCreativeDesc VARCHAR(1024), 
	IN vExtCreativeKey VARCHAR(128), 
	IN vChannelId BIGINT, 
	IN vCreative TEXT, 
	IN vHeader TEXT, 
	IN vTemplatePersonalizationScore TINYINT, 
	IN vOfferId BIGINT, 
	INOUT vCreativeId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Creative';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCreativeName IS NULL OR vCreativeName = '' THEN
		SET vErrMsg = 'Creative Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vCreativeId IS NULL OR vCreativeId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Creative (
					CreativeName, 
					CreativeDesc, 
					ChannelId, 
					ExtCreativeKey,
					Creative,
					Header,
					TemplatePersonalizationScore,
					OfferId,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				, '"', vCreativeName, '"'
				, ',"', vCreativeDesc, '"' 
				, ',', vChannelId
				, ',"', vExtCreativeKey, '"' 
				, ',"', vCreative, '"' 
				, ',"', vHeader, '"' 
				, ',', vTemplatePersonalizationScore
				, ',', vOfferId
				, ',', vUserId
				, ',', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;
			            
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
			
				
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events
				WHERE 
					CreativeId = vCreativeId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Creative (
						CreativeId,
						CreativeName, 
						CreativeDesc, 
						ChannelId, 
						ExtCreativeKey,
						Creative,
						Header,
						TemplatePersonalizationScore,
						OfferId,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
						, vCreativeId
						, ',"', vCreativeName, '"'
						, ',"', vCreativeDesc, '"' 
						, ',', vChannelId
						, ',"', vExtCreativeKey, '"' 
						, ',"', vCreative, '"' 
						, ',"', vHeader, '"' 
						, ',', vTemplatePersonalizationScore
						, ',', vOfferId
						, ',', vUserId
						, ',', vUserId
						,') ON DUPLICATE KEY UPDATE '
						,' CreativeDesc = "', vCreativeDesc, '"'
						,', ChannelId = ', vChannelId
						,', ExtCreativeKey = "', vExtCreativeKey, '"'
						,', Creative = "', vCreative, '"'
						,', Header = "', vHeader, '"'
						,', TemplatePersonalizationScore = ', vTemplatePersonalizationScore
						,', OfferId = ', vOfferId
						,', CreatedBy = ',vUserId
						,', ModifiedBy = ',vUserId
					);

				SET vErrMsg = vSqlstmt;
                CALL CDM_Update_Process_Log('PROC_CLM_CU_CLM_Creative', 7, vErrMsg , vSuccess );
				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Creative already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
                CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT CreativeId INTO vCreativeId 
	FROM CLM_Creative
	WHERE CreativeName = vCreativeName
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Creative_PFields`(
	IN vUserId BIGINT, 
	IN vCreativeId BIGINT ,
	IN vPFieldName VARCHAR(64) ,
	IN vPFieldView VARCHAR(64) ,
	IN vPFieldColumn VARCHAR(64) ,
	IN vPFieldDefaultValue VARCHAR(1024) ,
	IN vPFieldAttributes TEXT,
	IN vPFieldQualConditionAsInUI TEXT,
	IN vPFieldQualConditionQuery TEXT,
	IN vPFieldRecoChecksAsInUI TEXT,
	IN vPFieldRecoChecksAsInUIQuery TEXT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Creative_PFields';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCreativeId > 0 THEN
		SELECT EXISTS(
			SELECT 1 
			FROM CLM_Campaign_Events
			WHERE 
				CreativeId = vCreativeId
				AND Has_Records_In_Execution_Hist = 1
		) INTO vRowCnt;
		IF vRowCnt = 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Creative_PFields (
					CreativeId,
					PFieldName,
					PFieldView,
					PFieldColumn,
					PFieldDefaultValue,
					PFieldAttributes,
					PFieldQualConditionAsInUI,
					PFieldQualConditionQuery,
					PFieldRecoChecksAsInUI,
					PFieldRecoChecksAsInUIQuery,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				, vCreativeId
				,',"', vPFieldName, '"'
				,',"', vPFieldView, '"'
				,',"', vPFieldColumn, '"'
				,',"', vPFieldDefaultValue, '"'
				,',"', vPFieldAttributes, '"'
				,',"', vPFieldQualConditionAsInUI, '"'
				,',"', vPFieldQualConditionQuery, '"'
				,',"', vPFieldRecoChecksAsInUI, '"'
				,',"', vPFieldRecoChecksAsInUIQuery, '"'
				,',',vUserId
				,',',vUserId
				,') ON DUPLICATE KEY UPDATE '
				,'	PFieldView = "', vPFieldView, '"'
				,',	PFieldColumn = "', vPFieldColumn, '"'
				,',	PFieldDefaultValue = "', vPFieldDefaultValue, '"'
				,',	PFieldAttributes = "', vPFieldAttributes, '"'
				,',	PFieldQualConditionAsInUI = "', vPFieldQualConditionAsInUI, '"'
				,',	PFieldQualConditionQuery = "', vPFieldQualConditionQuery, '"'
				,',	PFieldRecoChecksAsInUI = "', vPFieldRecoChecksAsInUI, '"'
				,',	PFieldRecoChecksAsInUIQuery = "', vPFieldRecoChecksAsInUIQuery, '"'
				,',	CreatedBy = ', vUserId
				,',	ModifiedBy = ', vUserId
				);
		
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
		END IF;
	END IF;
	
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Exclusion_Filter`(
	IN vUserId BIGINT, 
	IN vEFName VARCHAR(128), 
	IN vEFDesc VARCHAR(1024), 
	IN vEFChannelId BIGINT, 
	IN vEFTReplacedQueryAsInUI TEXT, 
	IN vEFReplacedQuery TEXT, 
	IN vIsValidSVOCCondition TINYINT, 
	IN vEFReplacedQueryBillAsInUI TEXT, 
	IN vEFReplacedQueryBill TEXT, 
	IN vIsValidBillCondition TINYINT, 
	IN vEFState TINYINT, 
	IN vEFSequence BIGINT, 
	INOUT vEFId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Exclusion_Filter';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vEFName IS NULL OR vEFName = '' THEN
		SET vErrMsg = 'EF Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE

		IF vEFId IS NULL OR vEFId <= 0 THEN
			SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Exclusion_Filter (
						EFName,
						EFDesc,
						EFChannelId,
						EFTReplacedQueryAsInUI,
						EFReplacedQuery,
						IsValidSVOCCondition,
						EFReplacedQueryBillAsInUI,
						EFReplacedQueryBill,
						IsValidBillCondition,
						EFState,
						EFSequence,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
						,'"',vEFName,'"'
						,',"',vEFDesc,'"'
						, ',',vEFChannelId
						,', "',vEFTReplacedQueryAsInUI,'"'
						,', "',vEFReplacedQuery,'"'
						, ',',vIsValidSVOCCondition
						,', "',vEFReplacedQueryBillAsInUI ,'"'
						, ',"',vEFReplacedQueryBill,'"'
						, ',',vIsValidBillCondition
						, ',',vEFState
						,', ',vEFSequence
						,', ',vUserId
						,', ',vUserId
					,') ON DUPLICATE KEY UPDATE '
					,' EFDesc = "', vEFDesc, '"'
					,', EFChannelId = ', vEFChannelId
					,', EFTReplacedQueryAsInUI = ','"', vEFTReplacedQueryAsInUI, '"'
					,', EFReplacedQuery = ','"', vEFReplacedQuery, '"'
					,', IsValidSVOCCondition = ', vIsValidSVOCCondition
					,', EFReplacedQueryBillAsInUI = ','"', vEFReplacedQueryBillAsInUI, '"'
					,', EFReplacedQueryBill = ','"', vEFReplacedQueryBill, '"'
					,', IsValidBillCondition = ', vIsValidBillCondition
					,', EFState = ', vEFState
					,', EFSequence = ', vEFSequence
					,', CreatedBy = ',vUserId
					,', ModifiedBy = ',vUserId
					,';'
				);

		ELSE
			SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Exclusion_Filter (
						EFId,
						EFName,
						EFDesc,
						EFChannelId,
						EFTReplacedQueryAsInUI,
						EFReplacedQuery,
						IsValidSVOCCondition,
						EFReplacedQueryBillAsInUI,
						EFReplacedQueryBill,
						IsValidBillCondition,
						EFState,
						EFSequence,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
						,vEFId
						,',"',vEFName,'"'
						,',"',vEFDesc,'"'
						, ',',vEFChannelId
						,', "',vEFTReplacedQueryAsInUI,'"'
						,', "',vEFReplacedQuery,'"'
						, ',',vIsValidSVOCCondition
						,', "',vEFReplacedQueryBillAsInUI ,'"'
						, ',"',vEFReplacedQueryBill,'"'
						, ',',vIsValidBillCondition
						, ',',vEFState
						,', ',vEFSequence
						,', ',vUserId
						,', ',vUserId
					,') ON DUPLICATE KEY UPDATE '
					,' EFDesc = "', vEFDesc, '"'
					,', EFChannelId = ', vEFChannelId
					,', EFTReplacedQueryAsInUI = ','"', vEFTReplacedQueryAsInUI, '"'
					,', EFReplacedQuery = ','"', vEFReplacedQuery, '"'
					,', IsValidSVOCCondition = ', vIsValidSVOCCondition
					,', EFReplacedQueryBillAsInUI = ','"', vEFReplacedQueryBillAsInUI, '"'
					,', EFReplacedQueryBill = ','"', vEFReplacedQueryBill, '"'
					,', IsValidBillCondition = ', vIsValidBillCondition
					,', EFState = ', vEFState
					,', EFSequence = ', vEFSequence
					,', CreatedBy = ',vUserId
					,', ModifiedBy = ',vUserId
					,';'
				);
		END IF;
	END IF;

	SET vErrMsg = vSqlstmt;
	
	SET @sql_stmt = vSqlstmt;
	PREPARE EXEC_STMT FROM @sql_stmt;
	EXECUTE EXEC_STMT;
    CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
	
	SELECT EFId INTO vEFId 
	FROM CLM_Exclusion_Filter
	WHERE EFName = vEFName
	LIMIT 1;
	 
	DEALLOCATE PREPARE EXEC_STMT;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_MicroSegment`(
	IN vUserId BIGINT, 
	IN vMicroSegmentName VARCHAR(128), 
	IN vMicroSegmentDesc VARCHAR(1024), 
	IN vMSReplacedQueryAsInUI TEXT,
	IN vMSReplacedQuery TEXT,
	IN vIsValidSVOCCondition TINYINT,
	IN vMSReplacedQueryBillAsInUI TEXT,
	IN vMSReplacedQueryBill TEXT,
	IN vIsValidBillCondition TINYINT,
	INOUT vMicroSegmentId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_MicroSegment';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vMicroSegmentName IS NULL OR vMicroSegmentName = '' THEN
		SET vErrMsg = 'MicroSegment Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vMicroSegmentId IS NULL OR vMicroSegmentId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_MicroSegment (
					MicroSegmentName, 
					MicroSegmentDesc,
					MSReplacedQueryAsInUI,
					MSReplacedQuery,
					IsValidSVOCCondition,
					MSReplacedQueryBillAsInUI,
					MSReplacedQueryBill,
					IsValidBillCondition,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				,'"', vMicroSegmentName,'"'
				,', "', vMicroSegmentDesc,'"'
				,', "', vMSReplacedQueryAsInUI,'"'
				,', "', vMSReplacedQuery,'"'
				,', ', vIsValidSVOCCondition
				,', "', vMSReplacedQueryBillAsInUI,'"'
				,', "', vMSReplacedQueryBill,'"'
				,', ', vIsValidBillCondition
				,', ', vUserId
				,', ', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
				
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events
				WHERE 
					MicroSegmentId = vMicroSegmentId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_MicroSegment (
						MicroSegmentId,
						MicroSegmentName, 
						MicroSegmentDesc,
						MSReplacedQueryAsInUI,
						MSReplacedQuery,
						IsValidSVOCCondition,
						MSReplacedQueryBillAsInUI,
						MSReplacedQueryBill,
						IsValidBillCondition,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vMicroSegmentId
					,', "', vMicroSegmentName,'"'
					,', "', vMicroSegmentDesc,'"'
					,', "', vMSReplacedQueryAsInUI,'"'
					,', "', vMSReplacedQuery,'"'
					,', ', vIsValidSVOCCondition
					,', "', vMSReplacedQueryBillAsInUI,'"'
					,', "', vMSReplacedQueryBill,'"'
					,', ', vIsValidBillCondition
					,', ', vUserId
					,', ', vUserId
					,') ON DUPLICATE KEY UPDATE '
					, '	MicroSegmentDesc = "',vMicroSegmentDesc, '"'
					,',	MSReplacedQueryAsInUI = "',vMSReplacedQueryAsInUI, '"'
					,',	MSReplacedQuery = "',vMSReplacedQuery, '"'
					,',	IsValidSVOCCondition = ', vIsValidSVOCCondition
					,',	MSReplacedQueryBillAsInUI = "',vMSReplacedQueryBillAsInUI, '"'
					,',	MSReplacedQueryBill = "',vMSReplacedQueryBill, '"'
					,',	IsValidBillCondition = ', vIsValidBillCondition
					,',	CreatedBy = ', vUserId
					,',	ModifiedBy = ', vUserId
					,';'
				);
				
				SET vErrMsg = vSqlstmt;

				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Micro Segement already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT MicroSegmentId INTO vMicroSegmentId 
	FROM CLM_MicroSegment
	WHERE MicroSegmentName = vMicroSegmentName
	LIMIT 1;
 
	SET vSuccess = 1;
	
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Offer`(
	IN vUserId BIGINT, 
	IN vOfferName VARCHAR(128), 
	IN vOfferDesc VARCHAR(1024), 
	IN vOfferTypeId BIGINT, 
	IN vOfferDef VARCHAR(2048),
	IN vOfferStartDate DATE, 
	IN vOfferEndDate DATE, 
	IN vOfferRedemptionDays INTEGER, 
	IN vOfferIsAtCustomerLevel TINYINT, 
	INOUT vOfferId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Offer';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vOfferName IS NULL OR vOfferName = '' THEN
		SET vErrMsg = 'Offer Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vOfferId IS NULL OR vOfferId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Offer (
					OfferName, 
					OfferDesc, 
					OfferTypeId, 
					OfferDef,
					OfferStartDate, 
					OfferEndDate, 
					OfferRedemptionDays, 
					OfferIsAtCustomerLevel,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				,'"', vOfferName,'"'
				,',"', vOfferDesc, '"'
				,',', vOfferTypeId
				,',"', vOfferDef,'"'
				,',"', vOfferStartDate,'"'
				,',"', vOfferEndDate, '"'
				,',', vOfferRedemptionDays
				,',', vOfferIsAtCustomerLevel
				,',', vUserId
				,',', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;

			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
				
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Offer (
						OfferId,
						OfferName, 
						OfferDesc, 
						OfferTypeId, 
						OfferDef,
						OfferStartDate, 
						OfferEndDate, 
						OfferRedemptionDays, 
						OfferIsAtCustomerLevel,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vOfferId
					,', "', vOfferName,'"'
					,',"', vOfferDesc, '"'
					,',', vOfferTypeId
					,',"', vOfferDef,'"'
					,',"', vOfferStartDate,'"'
					,',"', vOfferEndDate, '"'
					,',', vOfferRedemptionDays
					,',', vOfferIsAtCustomerLevel
					,',', vUserId
					,',', vUserId 
					,') ON DUPLICATE KEY UPDATE '
					,' OfferDesc = "', vOfferDesc, '"'
					,', OfferTypeId = ', vOfferTypeId
					,', OfferDef = "', vOfferDef, '"'
					,', OfferStartDate ="', vOfferStartDate, '"'
					,', OfferEndDate ="', vOfferEndDate, '"'
					,', OfferRedemptionDays = ', vOfferRedemptionDays
					,', OfferIsAtCustomerLevel = ', vOfferIsAtCustomerLevel
					,', CreatedBy = ',vUserId
					,', ModifiedBy = ',vUserId
				);
				
				SET vErrMsg = vSqlstmt;
				CALL CDM_Update_Process_Log('PROC_CLM_CU_CLM_Offer', 5, vErrMsg , vRowCnt );                
				SET @sql_stmt = vSqlstmt;
                select @sql_stmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Offer already exists and has been used in Campaign execution and hence cannot be modified.';
				SET vSuccess = 0;
				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);

			END IF;
		END IF;	
	END IF;
	    
	SELECT OfferId INTO vOfferId 
	FROM CLM_Offer
	WHERE OfferName = vOfferName
	LIMIT 1;
 
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);

END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_OfferType`(
	IN vUserId BIGINT, 
	IN vOfferTypeDesc VARCHAR(1024), 
	IN vOfferTypeName VARCHAR(128), 
	INOUT vOfferTypeId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;
	
	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_OfferType';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);
	
	IF vOfferTypeName IS NULL OR vOfferTypeName = '' THEN
		SET vErrMsg = 'OfferType Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vOfferTypeId IS NULL OR vOfferTypeId <= 0 THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_OfferType (
					OfferTypeName, 
					OfferTypeDesc, 
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				, '"', vOfferTypeName, '"'
				, ', "', vOfferTypeDesc, '"'
				, ', ', vUserId
				, ', ', vUserId
				, ')'
			);
			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
		
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_OfferType (
					OfferTypeId,
					OfferTypeName, 
					OfferTypeDesc, 
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				, vOfferTypeId
				, ',"', vOfferTypeName, '"'
				, ', "', vOfferTypeDesc, '"'
				, ', ', vUserId
				, ', ', vUserId
				, ') ON DUPLICATE KEY UPDATE '
				, ' OfferTypeDesc = "', vOfferTypeDesc, '"'
				, ', CreatedBy = ', vUserId
				, ', ModifiedBy = ', vUserId
			);
			
			SET vErrMsg = vSqlstmt;
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
		END IF;
	END IF;
	
	SELECT OfferTypeId INTO vOfferTypeId 
	FROM CLM_OfferType
	WHERE OfferTypeName = vOfferTypeName
	LIMIT 1;
	
	SET vSuccess = 1;
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Reco_In_Creative`(
	IN vUserId BIGINT, 
	IN vCreativeId BIGINT , 
	IN vExclude_Recommended_SameAP_NotLT_FavCat TINYINT, 
	IN vExclude_Recommended_SameLTD_FavCat TINYINT, 
	IN vExclude_Recommended_SameLT_FavCat TINYINT, 
	IN vRecommendation_Column VARCHAR(2048), 
	IN vRecommendation_Type VARCHAR(2048),
	IN vExclude_Stock_out TINYINT, 
	IN vExclude_Transaction_X_Days TINYINT, 
	IN vExclude_Recommended_X_Days TINYINT, 
	IN vExclude_Never_Brought TINYINT, 
	IN vExclude_No_Image TINYINT, 
	IN vNumber_Of_Recommendation INTEGER, 
	IN vRecommendation_Filter_Logic VARCHAR(2048),
	IN vRecommendation_Column_Header VARCHAR(2048),
	IN vIs_Recommended_SameLT_FavCat TINYINT, 
	IN vIs_Recommended_SameLTD_Cat TINYINT, 
	IN vIs_Recommended_SameLTD_Dep TINYINT, 
	IN vIs_Recommended_NewLaunch_Product TINYINT, 
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT

)
BEGIN
	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Reco_In_Creative';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCreativeId > 0 THEN
		SELECT EXISTS(
			SELECT 1 
			FROM CLM_Campaign_Events
			WHERE 
				CreativeId = vCreativeId
				AND Has_Records_In_Execution_Hist = 1
		) INTO vRowCnt;
		IF vRowCnt = 0 THEN

			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Reco_In_Creative (
					CreativeId,
					Exclude_Recommended_SameAP_NotLT_FavCat,
					Exclude_Recommended_SameLTD_FavCat,
					Exclude_Recommended_SameLT_FavCat,
					Recommendation_Column,
					Recommendation_Type,
					Exclude_Stock_out,
					Exclude_Transaction_X_Days,
					Exclude_Recommended_X_Days,
					Exclude_Never_Brought,
					Exclude_No_Image,
					Number_Of_Recommendation,
					Recommendation_Filter_Logic,
					Recommendation_Column_Header,
					Is_Recommended_SameLT_FavCat,
					Is_Recommended_SameLTD_Cat,
					Is_Recommended_SameLTD_Dep,
					Is_Recommended_NewLaunch_Product,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				, vCreativeId
				,',',vExclude_Recommended_SameAP_NotLT_FavCat
				,',',vExclude_Recommended_SameLTD_FavCat
				,',',vExclude_Recommended_SameLT_FavCat
				,',"', vRecommendation_Column, '"'
				,',"', vRecommendation_Type, '"'
				,',',vExclude_Stock_out
				,',',vExclude_Transaction_X_Days
				,',',vExclude_Recommended_X_Days
				,',',vExclude_Never_Brought
				,',',vExclude_No_Image
				,',',vNumber_Of_Recommendation
				,',"', vRecommendation_Filter_Logic, '"'
				,',"', vRecommendation_Column_Header, '"'
				,',',vIs_Recommended_SameLT_FavCat
				,',',vIs_Recommended_SameLTD_Cat
				,',',vIs_Recommended_SameLTD_Dep
				,',',vIs_Recommended_NewLaunch_Product
				,',',vUserId
				,',',vUserId
				,') ON DUPLICATE KEY UPDATE '
				,'	Exclude_Recommended_SameAP_NotLT_FavCat = ', vExclude_Recommended_SameAP_NotLT_FavCat
				,',	Exclude_Recommended_SameLTD_FavCat = ', vExclude_Recommended_SameLTD_FavCat
				,',	Exclude_Recommended_SameLT_FavCat = ', vExclude_Recommended_SameLT_FavCat
				,',	Recommendation_Column = "', vRecommendation_Column, '"'
				,',	Recommendation_Type = "', vRecommendation_Type, '"'
				,',	Exclude_Stock_out = ', vExclude_Stock_out
				,',	Exclude_Transaction_X_Days = ', vExclude_Transaction_X_Days
				,',	Exclude_Recommended_X_Days = ', vExclude_Recommended_X_Days
				,',	Exclude_Never_Brought = ', vExclude_Never_Brought
				,',	Exclude_No_Image = ', vExclude_No_Image
				,',	Number_Of_Recommendation = ', vNumber_Of_Recommendation
				,',	Recommendation_Filter_Logic = "', vRecommendation_Filter_Logic, '"'
				,',	Recommendation_Column_Header = "', vRecommendation_Column_Header, '"'
				,',	Is_Recommended_SameLT_FavCat = ', vIs_Recommended_SameLT_FavCat
				,',	Is_Recommended_SameLTD_Cat = ', vIs_Recommended_SameLTD_Cat
				,',	Is_Recommended_SameLTD_Dep = ', vIs_Recommended_SameLTD_Dep
				,',	Is_Recommended_NewLaunch_Product = ', vIs_Recommended_NewLaunch_Product
				,',	CreatedBy = ', vUserId
				,',	ModifiedBy = ', vUserId
				);
				
			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;
            SET vSuccess = 1;
			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
		END IF;
	END IF;
	
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_Segment`(
	IN vUserId BIGINT, 
	IN vCLMSegmentName VARCHAR(128), 
	IN vCLMSegmentDesc VARCHAR(1024), 
	IN vCLMSegmentReplacedQueryAsInUI TEXT, 
	IN vCLMSegmentReplacedQuery TEXT, 
	IN vIsValidCondition TINYINT,	
	INOUT vCLMSegmentId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN

	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;

	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_Segment';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCLMSegmentName IS NULL OR vCLMSegmentName = '' THEN
		SET vErrMsg = 'Segment Name is a Mandatory Field and cannot be empty or NULL';
		SET vSuccess = 0;
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);
	ELSE
		IF vCLMSegmentId IS NULL OR vCLMSegmentId <= 0  THEN
			SET vSqlstmt = CONCAT(
				'INSERT IGNORE INTO CLM_Segment (
					CLMSegmentName, 
					CLMSegmentDesc, 
					CLMSegmentReplacedQueryAsInUI,
					CLMSegmentReplacedQuery,
					IsValidCondition,
					CreatedBy, 
					ModifiedBy
				) VALUES ('
				, '"' , vCLMSegmentName,'"'
				, ', "', vCLMSegmentDesc,'"'
				, ', "', vCLMSegmentReplacedQueryAsInUI,'"'
				, ', "', vCLMSegmentReplacedQuery,'"'
				, ', ', vIsValidCondition
				, ', ', vUserId
				,', ', vUserId
				, ')'
			);

			SET vErrMsg = vSqlstmt;

			SET @sql_stmt = vSqlstmt;
			PREPARE EXEC_STMT FROM @sql_stmt;
			EXECUTE EXEC_STMT;
			DEALLOCATE PREPARE EXEC_STMT;

			CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 3, vErrMsg, vSuccess);
		ELSE
			
			SELECT EXISTS(
				SELECT 1 
				FROM CLM_Campaign_Events 
				WHERE 
					CLMSegmentId = vCLMSegmentId
					AND Has_Records_In_Execution_Hist = 1
			) INTO vRowCnt;
			
			IF vRowCnt = 0 THEN
				SET vSqlstmt = CONCAT(
					'INSERT IGNORE INTO CLM_Segment (
						CLMSegmentId,
						CLMSegmentName, 
						CLMSegmentDesc, 
						CLMSegmentReplacedQueryAsInUI,
						CLMSegmentReplacedQuery,
						IsValidCondition,
						CreatedBy, 
						ModifiedBy
					) VALUES ('
					, vCLMSegmentId
					, ', "' , vCLMSegmentName,'"'
					, ', "', vCLMSegmentDesc,'"'
					, ', "', vCLMSegmentReplacedQueryAsInUI,'"'
					, ', "', vCLMSegmentReplacedQuery,'"'
					, ', ', vIsValidCondition
					, ', ', vUserId
					,', ', vUserId
					, ') ON DUPLICATE KEY UPDATE '
					, '  CLMSegmentDesc = "', vCLMSegmentDesc, '"'
					, ', CLMSegmentReplacedQueryAsInUI = "', vCLMSegmentReplacedQueryAsInUI, '"'
					, ', CLMSegmentReplacedQuery = "', vCLMSegmentReplacedQuery, '"'
					, ', IsValidCondition = ', vIsValidCondition
					, ', CreatedBy = ', vUserId
					, ',  ModifiedBy = ', vUserId
					,';'
				);
				
				SET vErrMsg = vSqlstmt;

				SET @sql_stmt = vSqlstmt;
				PREPARE EXEC_STMT FROM @sql_stmt;
				EXECUTE EXEC_STMT;
				DEALLOCATE PREPARE EXEC_STMT;

				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 4, vErrMsg, vSuccess);
			ELSE
				SET vErrMsg = 'The Life Cycle Segment already exists and has been used in Campaigns and hence cannot be modified.';
				SET vSuccess = 0;

				CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 5, vErrMsg, vSuccess);
			END IF;
		END IF;
	END IF;
	
	SELECT CLMSegmentId INTO vCLMSegmentId 
	FROM CLM_Segment
	WHERE CLMSegmentName = vCLMSegmentName
	LIMIT 1;

	SET vSuccess = 1;

	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 6, vErrMsg, vSuccess);
END


|
CREATE OR REPLACE PROCEDURE `PROC_CLM_CU_CLM_User`(
	IN vUserId VARCHAR(128), 
	IN vCreator_SOLUS_UID BIGINT,  
	INOUT vSOLUS_UID BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vRowCnt BIGINT DEFAULT 0;
	DECLARE vSqlstmt TEXT;
	
	DECLARE EXIT HANDLER FOR SQLEXCEPTION 
	BEGIN 
		SET vSuccess = 0; 
		GET DIAGNOSTICS CONDITION 1 @sqlstate = RETURNED_SQLSTATE, @errno = MYSQL_ERRNO, @text = MESSAGE_TEXT;
		SET vErrMsg = CONCAT(IFNULL(vErrMsg,''), "ERROR ", @errno, " (", @sqlstate, "): ", @text);
		CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 0, vErrMsg, 0);
	END;

	SET vErrMsg = 'Start';
	SET vSuccess = 0;
	SET @CURR_PROC_NAME = 'PROC_CLM_CU_CLM_User';
	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 1, vErrMsg, vSuccess);

	IF vCreator_SOLUS_UID <= 0 THEN 
		SET vCreator_SOLUS_UID = 1;
	END IF;
    
	SET vSqlstmt = CONCAT(
			'INSERT IGNORE INTO CLM_User (UserId, CreatedBy, ModifiedBy ) VALUES ('
			,'"'
            , vUserId
			, '",'
			,vCreator_SOLUS_UID
			, ","
			, vCreator_SOLUS_UID
			,');'
		);

	SET vErrMsg = vSqlstmt;
	
	SET @sql_stmt = vSqlstmt;
	PREPARE EXEC_STMT FROM @sql_stmt;
	EXECUTE EXEC_STMT;
	 
	SELECT SOLUS_UID INTO vSOLUS_UID 
	FROM CLM_User
	WHERE UserId = vUserId
	LIMIT 1;
 
	DEALLOCATE PREPARE EXEC_STMT;
	
	SET vSuccess = 1;

	CALL CDM_Update_Process_Log(@CURR_PROC_NAME, 2, vErrMsg, vSuccess);

END
