CREATE OR REPLACE PROCEDURE `PROC_UI_CLM_Ondemand_Trigger`(
	IN OnDemandEventId BIGINT(20),           
    OUT Msg Text                           
 )
Begin

	DECLARE Load_Exe_ID, Exe_ID int;
    
set Load_Exe_ID = F_Get_Load_Execution_Id();

  set SQL_SAFE_UPDATES=0;
	set foreign_key_checks=0;

insert into log_solus(module,rec_start,rec_end,msg)   values ('S_CLM_Ondemand',OnDemandEventId,'0','CLM has started as ON demand mode');

insert  into ETL_Execution_Details (Procedure_Name, Job_Name, Start_Time, End_Time,`Status`) 
	values ('S_CLM_Ondemand', OnDemandEventId,'0000-00-00 00:00:00','0000-00-00 00:00:00','CLM has started as ON demand mode');    
         
    
    set Exe_ID = F_Get_Execution_Id();
    
SELECT  `Value` INTO @V_Schema_Name FROM M_Config WHERE 
		 Name = 'Tenant_Name'
			;
    

commit;

    select @V_Schema_Name;
set @OnDemandEventId=OnDemandEventId;
set @Run_Mode='RUN';

 set @CLM = concat( ' call ',@V_Schema_Name,' .S_CLM_Run(',@OnDemandEventId,',',"@Run_Mode",',@no_of_customers,@no_of_customers_final, @Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,
 @RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg)');
SELECT @CLM;
														prepare EventExecution from @CLM;
														execute EventExecution;
														deallocate prepare EventExecution;
														

select
@no_of_customers,@no_of_customers_final, @Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,
 @RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg;
 
 SELECT 
    CONCAT(@vSuccess,'',@vErrMsg) into Msg
    ;

 select max(Load_Execution_ID) into @Load_Exe_ID from ETL_Execution_Details where Procedure_Name = 'S_CLM_Ondemand';
 
 select max(Execution_ID) into @Exe_ID from ETL_Execution_Details where Procedure_Name = 'S_CLM_Ondemand';

UPDATE ETL_Execution_Details 
SET 
    Status = 'Succeeded',
    End_Time = NOW()
WHERE
    Procedure_Name = 'S_CLM_Ondemand'
        AND Load_Execution_ID = @Load_Exe_ID
        AND Execution_ID = @Exe_ID;



update  ETL_Execution_Details
set Start_Time=now(),End_Time=now()
where Procedure_Name='S_CLM_Ondemand'
and Start_Time ='0000-00-00 00:00:00';



END


|
CREATE OR REPLACE PROCEDURE `PROC_UI_CLM_S_Generate_Comm_Template`(
	IN v_pfields VARCHAR(512), 
	IN sms_flag CHAR(1), 
	IN pn_flag CHAR(1), 
	IN email_flag CHAR(1), 
	OUT Comm_Header VARCHAR(512), 
	OUT Comm_Template VARCHAR (512)
   ,OUT vSuccess int (4), 
    OUT vErrMsg Text
)
BEGIN
	DECLARE  v_Del VARCHAR(12) DEFAULT ',';

	DECLARE  v_emailStr VARCHAR(64) DEFAULT '\{\"Email\":[';
	DECLARE  v_smsStr VARCHAR(64) DEFAULT '],\"SMS\":[';
	DECLARE  v_pnStr VARCHAR(64) DEFAULT '],\"PN\":[';
	DECLARE  v_closingStr VARCHAR(64) DEFAULT ']}';
	DECLARE  v_sms_header VARCHAR(512) DEFAULT '';
	DECLARE  v_pn_header VARCHAR(512) DEFAULT '';
	DECLARE  v_email_header VARCHAR(512) DEFAULT '';
	DECLARE  v_HeaderValues VARCHAR(512) DEFAULT '';

	DECLARE  v_emailstart VARCHAR(64) DEFAULT '\"<Email>';
	DECLARE  v_emailend VARCHAR(64) DEFAULT '</Email>';
	DECLARE  v_smsstart VARCHAR(64) DEFAULT '<SMS>';
	DECLARE  v_smsend VARCHAR(64) DEFAULT '</SMS>';
	DECLARE  v_pnstart VARCHAR(64) DEFAULT '<PN>';
	DECLARE  v_pnend VARCHAR(64) DEFAULT '</PN>\"';
	DECLARE  v_sms_template VARCHAR(512) DEFAULT '';
	DECLARE  v_pn_template VARCHAR(512) DEFAULT '';
	DECLARE  v_email_template VARCHAR(512) DEFAULT '';
	DECLARE  v_TemplateValues VARCHAR(512) DEFAULT '';
	
	DECLARE Load_Exe_ID, Exe_ID int;
    
    
	Declare done,done2 INT DEFAULT FALSE;
    Declare y longtext;


                
Declare cur1 CURSOR for 

select distinct concat(substring(a,1,7),concat(substring(replace(a,'|',','),8)) )from (

select concat('select cdv.Customer_Id,concat(',Comm_Template,') 
from Customer_Details_View cdv where Customer_Id<100','') as a from
 Customer_Details_View
 where Customer_Id<100
) b;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

   
	 SET vSuccess = 0;
	SET vErrMsg = CONCAT('\r\n','Please check the Template properly ');
    
	SELECT 
		CONCAT('\'',
            GROUP_CONCAT(item
                SEPARATOR "','"),
            '\'') INTO v_HeaderValues 
	FROM (
		SELECT 
			position
			, TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(v_pfields, v_Del, position + 1), v_Del, - 1)) AS `item`
		FROM (
			SELECT 
				n AS `position`
			FROM (
				SELECT 0 AS `n` 
				UNION ALL 
				SELECT 1 AS `1` 
				UNION ALL 
				SELECT 2 AS `2` 
				UNION ALL 
				SELECT 3 AS `3` 
				UNION ALL 
				SELECT 4 AS `4` 
				UNION ALL 
				SELECT 5 AS `5` 
				UNION ALL 
				SELECT 6 AS `6` 
				UNION ALL 
				SELECT 6 AS `7` 
				UNION ALL 
				SELECT 6 AS `8` 
				UNION ALL 
				SELECT 6 AS `9` 
				UNION ALL 
				SELECT 6 AS `10` 
				UNION ALL 
				SELECT 6 AS `11` 
				UNION ALL 
				SELECT 6 AS `12` 
				UNION ALL 
				SELECT 6 AS `13` 
				UNION ALL 
				SELECT 6 AS `14` 
				UNION ALL 
				SELECT 6 AS `15` 
				UNION ALL 
				SELECT 6 AS `16`
			) AS vw
		) AS tallyGenerator
	WHERE
		position <= (CHAR_LENGTH(v_pfields) - CHAR_LENGTH(REPLACE(v_pfields, v_Del, '')))) delimitedString;


	IF sms_flag = 'Y' THEN
		SET  v_sms_header = v_HeaderValues;
	END IF;
	IF pn_flag = 'Y' THEN
		SET  v_pn_header = v_HeaderValues;
	END IF ;
	IF email_flag = 'Y' THEN
		SET v_email_header  = v_HeaderValues;
	END IF;

	SELECT replace(replace(CONCAT(v_emailStr, v_email_header ,v_smsStr, v_sms_header,v_pnStr, v_pn_header, v_closingStr),"'",'"'),'"','\\"') INTO Comm_Template;
	
 
select  Comm_Template;
	
	SELECT 
		GROUP_CONCAT(CONCAT('"|ifnull(', item, ',"")|"') SEPARATOR ',')	INTO v_TemplateValues 
	FROM (
		SELECT 
			position
            ,TRIM(SUBSTRING_INDEX(SUBSTRING_INDEX(v_pfields, v_Del, position + 1), v_Del, - 1)) AS `item`
		FROM (
			SELECT 
				n AS `position`
			FROM (
				SELECT 0 AS `n` 
				UNION ALL 
				SELECT 1 AS `1` 
				UNION ALL 
				SELECT 2 AS `2` 
				UNION ALL 
				SELECT 3 AS `3` 
				UNION ALL 
				SELECT 4 AS `4` 
				UNION ALL 
				SELECT 5 AS `5` 
				UNION ALL 
				SELECT 6 AS `6` 
				UNION ALL 
				SELECT 6 AS `7` 
				UNION ALL 
				SELECT 6 AS `8` 
				UNION ALL 
				SELECT 6 AS `9` 
				UNION ALL 
				SELECT 6 AS `10` 
				UNION ALL 
				SELECT 6 AS `11` 
				UNION ALL 
				SELECT 6 AS `12` 
				UNION ALL 
				SELECT 6 AS `13` 
				UNION ALL 
				SELECT 6 AS `14` 
				UNION ALL 
				SELECT 6 AS `15` 
				UNION ALL 
				SELECT 6 AS `16`
			) AS vw
		) AS tallyGenerator
    WHERE
        position <= (CHAR_LENGTH(v_pfields) - CHAR_LENGTH(REPLACE(v_pfields, v_Del, '')))) delimitedString;

	IF sms_flag = 'Y' THEN
		SET  v_sms_template = v_TemplateValues;
	END IF;

	IF pn_flag = 'Y' THEN
		SET  v_pn_template = v_TemplateValues;
	END IF ;

	IF email_flag = 'Y' THEN
		SET v_email_template  = v_TemplateValues;
	END IF;

select CONCAT( v_emailstart ,v_email_template,v_emailend,v_smsstart,v_sms_template,v_smsend
            ,v_pnstart,v_pn_template,v_pnend) into @Comm_Template;
			 select @Comm_Template;

	SET Comm_Header=   
    replace (CONCAT( v_emailstart ,v_email_template,v_emailend,v_smsstart,v_sms_template,v_smsend
            ,v_pnstart,v_pn_template,v_pnend),'"','\\"');
           SELECT Comm_Header;
           
   if v_pfields='' then
    set Comm_Header='\\"<Email></Email><SMS></SMS><PN></PN>\\"';
	set  Comm_Template='{\\"Email\\":[],\\"SMS\\":[],\\"PN\\":[]}'; 
	set @Comm_Template='"<Email></Email><SMS></SMS><PN></PN>"';
       end if;    
        select @Comm_Template;
		call PROC_CLM_IS_VALID_Template(@Comm_Template,@vSuccess,@vErrMsg);  
		
      SELECT Comm_Template,Comm_Header;
    
    IF 	@vSuccess =1 Then
		select  @vSuccess into vSuccess ;
		select @vErrMsg into  vErrMsg ;
    ELSE 
			select  @vSuccess into vSuccess ;
		select @vErrMsg into  vErrMsg ;
   END IF;
   
    
END


|
CREATE or replace PROCEDURE `PROC_UI_CLM_Schedule`(
    OUT Msg Text                           
 )
Begin


SELECT  `Value` INTO @V_Schema_Name FROM M_Config WHERE 
		 Name = 'Tenant_Name'
			;
    
    
    
    select @V_Schema_Name;
set @OnDemandEventId='';
set @Run_Mode='ALL';


 set @CLM = concat( ' call ',@V_Schema_Name,' .S_CLM_Run(',@OnDemandEventId,',',"@Run_Mode",',@no_of_customers,@no_of_customers_final, @Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,
 @RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg)');
SELECT @CLM;
														prepare EventExecution from @CLM;
														execute EventExecution;
														deallocate prepare EventExecution;
														


select
@no_of_customers,@no_of_customers_final, @Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,
 @RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg;
 
 SELECT 
    CONCAT(@vSuccess,'',@vErrMsg) into Msg
    ;

END


|
CREATE or replace PROCEDURE `PROC_UI_CLM_Test`(
	IN OnDemandEventId BIGINT(20),             

 OUT no_of_customers BIGINT(20),           
 OUT no_of_customers_final BIGINT(20),     
 OUT Filename Text ,                       
 OUT FileHeader Text,                      
 Out FileLocation Text,                    
 OUT RecoFilename Text,                    
 OUT RecoFileHeader Text,                  
 OUT RecoFileRecoHeader Text,              
 OUT RecoFileLocation Text,                 
OUT vSuccess int (4),                      
OUT vErrMsg Text                           
 
)
Begin

CALL S_CLM_Run(OnDemandEventId,'TEST', @no_of_customers,@no_of_customers_final, @Filename,@FileHeader,@FileLocation,@RecoFilename,@RecoFileHeader,
 @RecoFileRecoHeader,@RecoFileLocation,@vSuccess,@vErrMsg);


 select Recommendation_Column into RecoFileRecoHeader
 From Event_Master A, Communication_Template B where A.Communication_Template_Id=B.Id and A.Id=OnDemandEventId;
 
 select RecoFileRecoHeader;
 If  RecoFileRecoHeader<>''
	THEN 
    
	SELECT @Filename  INTO RecoFilename;
SELECT @FileHeader INTO RecoFileHeader;
SELECT @FileLocation INTO RecoFileLocation;
SELECT ''INTO Filename;
SELECT ''INTO FileHeader;
SELECT '' INTO FileLocation;


ELSE 

SELECT @Filename INTO Filename;
SELECT @FileHeader INTO FileHeader;
SELECT @FileLocation INTO FileLocation;
SELECT ''  INTO RecoFilename;
SELECT '' INTO RecoFileHeader;
SELECT '' INTO RecoFileLocation;
END IF;

	
	
SELECT @no_of_customers INTO no_of_customers;
SELECT @no_of_customers_final INTO no_of_customers_final;


SELECT @vSuccess INTO vSuccess;
select @vErrMsg into vErrMsg;

END


|
CREATE or replace PROCEDURE `PROC_UI_CLM_Test_Event`(
	IN vUserId BIGINT,
	IN vEventId BIGINT,
	OUT vOutMsg TEXT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	CALL S_Event_Execution_Solus50();
	CALL S_Unique_Offer_Code_Assignment();
	CALL S_Personalized_Communication();
	CALL S_Communication_File();



END


|
CREATE or replace PROCEDURE `PROC_UI_Create_Event`(

	IN vUserId BIGINT,   
	IN vCampaignThemeId BIGINT, 
	IN vCLMSegmentId BIGINT, 
	IN vCampaignId BIGINT, 
	IN vMicroSegmentId BIGINT, 
	IN vChannelId BIGINT, 
	IN vOfferTypeId BIGINT,  

	IN vOfferName VARCHAR(128), 
	IN vOfferDesc VARCHAR(1024),
	IN vOfferDef VARCHAR(2048),
	IN vOfferStartDate DATE,
	IN vOfferEndDate DATE,
	IN vOfferRedemptionDays INTEGER,
	IN vOfferIsAtCustomerLevel TINYINT,

	IN vCampTriggerName VARCHAR(128), 
	IN vCampTriggerDesc VARCHAR(1024),
	IN vPrecedingTriggerName VARCHAR(128), 
	IN vPrecedingTriggerId BIGINT,
	IN vDaysOffsetFromPrecedingTrigger SMALLINT,
	IN vCTState TINYINT,
	IN vCTIsMandatory TINYINT,
	IN vIsAReminder TINYINT,
	IN vCTReplacedQueryAsInUI TEXT,
	IN vCTReplacedQuery TEXT,
	IN vCTIsValidSVOCCondition TINYINT,
	IN vCTReplacedQueryBillAsInUI TEXT,
	IN vCTReplacedQueryBill TEXT,
	IN vCTIsValidBillCondition TINYINT,
	IN vCTUsesGoodTimeModel	TINYINT,

	IN vCreativeName VARCHAR(128), 
	IN vExtCreativeKey VARCHAR(128),
	IN vCreativeDesc VARCHAR(1024),
	IN vCreative TEXT,
	IN vHeader TEXT,

	IN vExclude_Recommended_SameAP_NotLT_FavCat TINYINT,
	IN vExclude_Recommended_SameLTD_FavCat TINYINT,
	IN vExclude_Recommended_SameLT_FavCat TINYINT,
	IN vRecommendation_Column VARCHAR(2048),
	IN vRecommendation_Type VARCHAR(2048),
	IN vExclude_Stock_out TINYINT,
	IN vExclude_Transaction_X_Days TINYINT,
	IN vExclude_Recommended_X_Days TINYINT,
	IN vExclude_Never_Brought TINYINT,
	IN vExclude_No_Image TINYINT,
	IN vNumber_Of_Recommendation INTEGER,
	IN vRecommendation_Filter_Logic VARCHAR(2048),
	IN vRecommendation_Column_Header VARCHAR(2048),
	IN vIs_Recommended_SameLT_FavCat TINYINT,
	IN vIs_Recommended_SameLTD_Cat TINYINT,
	IN vIs_Recommended_SameLTD_Dep TINYINT,
	IN vIs_Recommended_NewLaunch_Product TINYINT,
	IN vEvent_Limit  BIGINT,

	INOUT vCampTriggerId BIGINT,
	INOUT vOfferId BIGINT,
	INOUT vCreativeId BIGINT,
	INOUT vEventId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
    
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			,' | CLMSegmentId: ',IFNULL(vCLMSegmentId,'NULL VALUE')
			,' | CampaignId: ',IFNULL(vCampaignId,'NULL VALUE')
			,' | MicroSegmentId: ',IFNULL(vMicroSegmentId,'NULL VALUE')
			,' | ChannelId: ',IFNULL(vChannelId,'NULL VALUE')
			,' | OfferTypeId: ',IFNULL(vOfferTypeId ,'NULL VALUE')
			,' | OfferName: ',IFNULL(vOfferName,'NULL VALUE')
			,' | OfferDesc: ',IFNULL(vOfferDesc,'NULL VALUE')
			,' | OfferDef: ',IFNULL(vOfferDef,'NULL VALUE')
			,' | OfferStartDate: ',IFNULL(vOfferStartDate,'NULL VALUE')
			,' | OfferEndDate: ',IFNULL(vOfferEndDate,'NULL VALUE')
			,' | OfferRedemptionDays: ',IFNULL(vOfferRedemptionDays,'NULL VALUE')
			,' | OfferIsAtCustomerLevel: ',IFNULL(vOfferIsAtCustomerLevel,'NULL VALUE')
			,' | CampTriggerName: ',IFNULL(vCampTriggerName,'NULL VALUE')
			,' | CampTriggerDesc: ',IFNULL(vCampTriggerDesc,'NULL VALUE')
			,' | PrecedingTriggerName: ',IFNULL(vPrecedingTriggerName,'NULL VALUE')
			,' | PrecedingTriggerId: ',IFNULL(vPrecedingTriggerId,'NULL VALUE')
			,' | DaysOffsetFromPrecedingTrigger: ',IFNULL(vDaysOffsetFromPrecedingTrigger,'NULL VALUE')
			,' | CTState: ',IFNULL(vCTState,'NULL VALUE')
			,' | CTIsMandatory: ',IFNULL(vCTIsMandatory,'NULL VALUE')
			,' | IsAReminder: ',IFNULL(vIsAReminder,'NULL VALUE')
			,' | CTReplacedQueryAsInUI: ',IFNULL(vCTReplacedQueryAsInUI,'NULL VALUE')
			,' | CTReplacedQuery: ',IFNULL(vCTReplacedQuery,'NULL VALUE')
			,' | CTIsValidSVOCCondition: ',IFNULL(vCTIsValidSVOCCondition,'NULL VALUE')
			,' | CTReplacedQueryBillAsInUI: ',IFNULL(vCTReplacedQueryBillAsInUI,'NULL VALUE')
			,' | CTReplacedQueryBill: ',IFNULL(vCTReplacedQueryBill,'NULL VALUE')
			,' | CTIsValidBillCondition: ',IFNULL(vCTIsValidBillCondition,'NULL VALUE')
			,' | CTUsesGoodTimeModel: ',IFNULL(vCTUsesGoodTimeModel,'NULL VALUE')
			,' | CreativeName: ',IFNULL(vCreativeName,'NULL VALUE')
			,' | ExtCreativeKey: ',IFNULL(vExtCreativeKey,'NULL VALUE')
			,' | CreativeDesc: ',IFNULL(vCreativeDesc,'NULL VALUE')
			,' | Creative: ',IFNULL(vCreative,'NULL VALUE')
			,' | Header: ',IFNULL(vHeader,'NULL VALUE')
			,' | Exclude_Recommended_SameAP_NotLT_FavCat: ',IFNULL(vExclude_Recommended_SameAP_NotLT_FavCat,'NULL VALUE')
			,' | Exclude_Recommended_SameLTD_FavCat: ',IFNULL(vExclude_Recommended_SameLTD_FavCat,'NULL VALUE')
			,' | Exclude_Recommended_SameLT_FavCat: ',IFNULL(vExclude_Recommended_SameLT_FavCat,'NULL VALUE')
			,' | Recommendation_Column: ',IFNULL(vRecommendation_Column,'NULL VALUE')
			,' | Recommendation_Type: ',IFNULL(vRecommendation_Type,'NULL VALUE')
			,' | Exclude_Stock_out: ',IFNULL(vExclude_Stock_out,'NULL VALUE')
			,' | Exclude_Transaction_X_Days: ',IFNULL(vExclude_Transaction_X_Days,'NULL VALUE')
			,' | Exclude_Recommended_X_Days: ',IFNULL(vExclude_Recommended_X_Days,'NULL VALUE')
			,' | Exclude_Never_Brought: ',IFNULL(vExclude_Never_Brought,'NULL VALUE')
			,' | Exclude_No_Image: ',IFNULL(vExclude_No_Image,'NULL VALUE')
			,' | Number_Of_Recommendation: ',IFNULL(vNumber_Of_Recommendation,'NULL VALUE')
			,' | Recommendation_Filter_Logic: ',IFNULL(vRecommendation_Filter_Logic,'NULL VALUE')
			,' | Recommendation_Column_Header: ',IFNULL(vRecommendation_Column_Header,'NULL VALUE')
			,' | Is_Recommended_SameLT_FavCat: ',IFNULL(vIs_Recommended_SameLT_FavCat,'NULL VALUE')
			,' | Is_Recommended_SameLTD_Cat: ',IFNULL(vIs_Recommended_SameLTD_Cat,'NULL VALUE')
			,' | Is_Recommended_SameLTD_Dep: ',IFNULL(vIs_Recommended_SameLTD_Dep,'NULL VALUE')
			,' | Is_Recommended_NewLaunch_Product: ',IFNULL(vIs_Recommended_NewLaunch_Product,'NULL VALUE')
			,' | CampTriggerId: ',IFNULL(vCampTriggerId,'NULL VALUE')
			,' | OfferId: ',IFNULL(vOfferId,'NULL VALUE')
			,' | CreativeId: ',IFNULL(vCreativeId,'NULL VALUE')
			,' | EventId: ',IFNULL(vEventId,'NULL VALUE')
			);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Campaign Theme cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCLMSegmentId IS NULL OR vCLMSegmentId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Life Cycle segment cannot be empty.');
		SET vSuccess = 0;
	END IF;
	
	IF vCampaignId IS NULL OR vCampaignId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Campaign cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vMicroSegmentId IS NULL OR vMicroSegmentId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Micro Segment cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vChannelId IS NULL OR vChannelId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Channel cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vOfferTypeId IS NULL OR vOfferTypeId <= 0 THEN
		SET vErrMsg = CONCAT(vErrMsg,'\r\n','Offer Type cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampTriggerId IS NULL OR vCampTriggerId <= 0 THEN
		
		IF vCampTriggerName IS NULL OR vCampTriggerName = '' THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','Trigger name cannot be empty.');
			SET vSuccess = 0;
		ELSE 
			SET vResult = 0;
			SELECT 1 INTO vResult FROM CLM_Campaign_Trigger WHERE CampTriggerName = vCampTriggerName;
			IF vResult = 1 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Trigger by the name ', vCampTriggerName, ' already exists.');
				SET vSuccess = 0;
			END IF;			
		END IF;
		IF (vCTReplacedQuery IS NULL OR vCTReplacedQuery = '')AND (vCTReplacedQueryBill IS NULL OR vCTReplacedQueryBill = '') THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','Both the SVOC criteria as well as Bill Criteria are empty. Please make sure at least one of them is valid.');
			SET vSuccess = 0;
		END IF;
		IF vCTState IS NULL OR vCTState < 0 THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Event status value is missing. This needs to be either enabled or disabled.');
			SET vSuccess = 0;
		END IF;
		IF vChannelId IS NULL OR vChannelId <= 0 THEN
			SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Channel for communication.');
			SET vSuccess = 0;
		END IF;
		IF vOfferTypeId > 1 THEN
			IF (vOfferId IS NULL OR vOfferId = 0) THEN
				IF vOfferName IS NULL OR vOfferName = '' THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide an Offer Name.');
					SET vSuccess = 0;
				
				
				END IF;
				IF vOfferStartDate IS NULL THEN 
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date cannot be empty.');
					SET vSuccess = 0;
				ELSE
					IF vOfferStartDate < CURRENT_DATE() THEN
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date has to be greater than Today.');
						SET vSuccess = 0;
					END IF;
				END IF;
				IF vOfferEndDate IS NULL THEN 
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be empty.');
					SET vSuccess = 0;
				ELSE
					IF vOfferStartDate < vOfferStartDate THEN
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be less than Offer');
						SET vSuccess = 0;
					END IF;
				END IF;
			END IF;
		END IF;

		IF vCreativeId	> 0 THEN
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName AND CreativeId <> vCreativeId;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
				
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Campaign_Events WHERE CreativeId = vCreativeId AND Has_Records_In_Execution_Hist = 1;
				IF vResult = 1 THEN							
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists and has been used in a SOLUS Run and hence cannot be modified.');
					SET vSuccess = 0;
				END IF;
			END IF;
		ELSE 
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
			END IF;
		END IF;

		if 	vNumber_Of_Recommendation >0 then
		
		IF (vRecommendation_Type IS NULL OR vRecommendation_Type <= 0) THEN
			IF vRecommendation_Column IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vNumber_Of_Recommendation IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vRecommendation_Filter_Logic IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vRecommendation_Column_Header IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vSuccess = 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Recommendation Type.');
			END IF;
		END IF;
		End if;
	ELSE 
		
		SET vResult = 0;
		SELECT EXISTS(
			SELECT 1 
			FROM CLM_Campaign_Events
			WHERE 
				CampTriggerId = vCampTriggerId
				AND Has_Records_In_Execution_Hist = 1
		) INTO vResult;
		IF vResult > 0 THEN
			SET vErrMsg = 'This Event has already been executed at least once as part of SOLUS run. Hence modification of the Event is not permitted.';
			SET vSuccess = 0;
		ELSE
			IF (vCTReplacedQuery IS NULL OR vCTReplacedQuery = '') AND (vCTReplacedQueryBill IS NULL OR vCTReplacedQueryBill = '') THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Both the SVOC criteria as well as Bill Criteria are empty. Please make sure at least one of them is valid.');
				SET vSuccess = 0;
			END IF;
			IF vCTState IS NULL OR vCTState < 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Event status value is missing. This needs to be either enabled or disabled.');
				SET vSuccess = 0;
			END IF;
			IF vChannelId IS NULL OR vChannelId <= 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Channel for communication.');
				SET vSuccess = 0;
			END IF;
			IF vOfferTypeId > 0 THEN
				IF (vOfferId IS NULL OR vOfferId <= 0) THEN
					IF vOfferName IS NULL OR vOfferName = '' THEN
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide an Offer Name.');
						SET vSuccess = 0;
							
					END IF;
					IF vOfferStartDate IS NULL THEN 
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date cannot be empty.');
						SET vSuccess = 0;
					ELSE
						IF vOfferStartDate < CURRENT_DATE() THEN
							SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer Start Date has to be greater than Today.');
							SET vSuccess = 0;
						END IF;
					END IF;
					IF vOfferEndDate IS NULL THEN 
						SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be empty.');
						SET vSuccess = 0;
					ELSE
						IF vOfferStartDate < vOfferStartDate THEN
							SET vErrMsg = CONCAT(vErrMsg,'\r\n','The Offer End Date cannot be less than Offer');
							SET vSuccess = 0;
						END IF;
					END IF;
				ELSE
					SET vResult = 0;
					SELECT EXISTS(
						SELECT 1 
						FROM CLM_Offer, CLM_Creative, CLM_Campaign_Events
						WHERE 
							CLM_Offer.OfferId = CLM_Creative.OfferId
							AND CLM_Creative.CreativeId = CLM_Campaign_Events.CreativeId
							AND CLM_Offer.OfferId = vOfferId
							AND CLM_Campaign_Events.Has_Records_In_Execution_Hist = 1
                             AND OfferName not  in ('CUSTOMER_LEVEL_OFFER','NO_OFFER')
					) INTO vResult;
					IF vResult > 0 THEN
						SET vErrMsg = 'This Offer has already been part of an execution at least once. Hence modification of the Offer is not permitted.';
						SET vSuccess = 0;
					END IF;
				END IF;
			END IF;
		END IF;

		IF vCreativeId	> 0 THEN
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName AND CreativeId <> vCreativeId;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
				
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Campaign_Events WHERE CreativeId = vCreativeId AND Has_Records_In_Execution_Hist = 1;
				IF vResult = 1 THEN							
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists and has been used in a SOLUS Run and hence cannot be modified.');
					SET vSuccess = 0;
				END IF;
			END IF;
		ELSE 
			IF vCreativeName IS NULL OR vCreativeName = '' THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please provide the Creative Name.');
				SET vSuccess = 0;
			ELSE
				SET vResult = 0;
				SELECT 1 INTO vResult FROM CLM_Creative WHERE CreativeName = vCreativeName;
				IF vResult = 1 THEN
					SET vErrMsg = CONCAT(vErrMsg,'\r\n','A Creative by the name ', vCreativeName, ' already exists.');						
					SET vSuccess = 0;
				END IF;
			END IF;
		END IF;

		if 	vNumber_Of_Recommendation >0 then
		
		IF (vRecommendation_Type IS NULL OR vRecommendation_Type <= 0) THEN
			IF vRecommendation_Column IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vNumber_Of_Recommendation IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vRecommendation_Filter_Logic IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vRecommendation_Column_Header IS NOT NULL THEN
				SET vSuccess = 0;
			END IF;
			IF vSuccess = 0 THEN
				SET vErrMsg = CONCAT(vErrMsg,'\r\n','Please select a Recommendation Type.');
			END IF;
		END IF;
		End if;


	END IF;

	IF vSuccess = 1 THEN
		SET vSuccess = 0;
		CALL PROC_CLM_Create_Event(
		
			vUserId,   
			vCampaignThemeId, 
			vCLMSegmentId, 
			vCampaignId, 
			vMicroSegmentId, 
			vChannelId, 
			vOfferTypeId,
		
			IFNULL(vOfferName,''),
			IFNULL(vOfferDesc,''),
			IFNULL(vOfferDef,''),
			IFNULL(vOfferStartDate,''),
			IFNULL(vOfferEndDate,''),
			IFNULL(vOfferRedemptionDays,0),
			IFNULL(vOfferIsAtCustomerLevel, 0),
		
			vCampTriggerName,
			IFNULL(vCampTriggerDesc,''),
			IFNULL(vPrecedingTriggerName,''),
			IFNULL(vPrecedingTriggerId ,0),
			IFNULL(vDaysOffsetFromPrecedingTrigger,0),
			IFNULL(vCTState, 0),
			IFNULL(vCTIsMandatory, 0),
			IFNULL(vIsAReminder, 0),
			IFNULL(vCTReplacedQueryAsInUI,''),
			IFNULL(vCTReplacedQuery,''),
			IFNULL(vCTIsValidSVOCCondition, 0),
			IFNULL(vCTReplacedQueryBillAsInUI,''),
			IFNULL(vCTReplacedQueryBill,''),
			IFNULL(vCTIsValidBillCondition, 0),
			IFNULL(vCTUsesGoodTimeModel,0),
		
			vCreativeName,
			IFNULL(vExtCreativeKey,''),
			IFNULL(vCreativeDesc,''),
			IFNULL(vCreative,''),
			IFNULL(vHeader,''),
		
			IFNULL(vExclude_Recommended_SameAP_NotLT_FavCat, 0),
			IFNULL(vExclude_Recommended_SameLTD_FavCat, 0),
			IFNULL(vExclude_Recommended_SameLT_FavCat, 0),
			IFNULL(vRecommendation_Column,''),
			IFNULL(vRecommendation_Type,''),
			IFNULL(vExclude_Stock_out, 0),
			IFNULL(vExclude_Transaction_X_Days, 0),
			IFNULL(vExclude_Recommended_X_Days, 0),
			IFNULL(vExclude_Never_Brought, 0),
			IFNULL(vExclude_No_Image, 0),
			IFNULL(vNumber_Of_Recommendation,0),
			IFNULL(vRecommendation_Filter_Logic,''),
			IFNULL(vRecommendation_Column_Header,''),
			IFNULL(vIs_Recommended_SameLT_FavCat, 0),
			IFNULL(vIs_Recommended_SameLTD_Cat, 0),
			IFNULL(vIs_Recommended_SameLTD_Dep, 0),
			IFNULL(vIs_Recommended_NewLaunch_Product, 0),
			IFNULL(vEvent_Limit,0),
		
			vCampTriggerId,
			vOfferId,
			vCreativeId,
			vEventId,
			vErrMsg,
			vSuccess
		);
	END IF;

	IF vSuccess = 0 THEN 
	SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Campaign`(
	IN vUserId BIGINT,
	IN vCampaignName VARCHAR(128),
	IN vCampaignDesc VARCHAR(1024),
	IN vCampaignThemeId BIGINT,
	IN vCLMSegmentId BIGINT,
	IN vCampaignCGPercentage DECIMAL(5,2),
	IN vCampaignResponseDays BIGINT,
	IN vCampaignState TINYINT,
	IN vCampaignSmartPtyState TINYINT,
	IN vCampaignIsOnDemand TINYINT,
	IN vCampaignUsesWaterfall TINYINT,
	IN vCampaignIsPerpetual TINYINT,
	IN vCampaignStartDate DATE,
	IN vCampaignEndDate DATE,
	IN vExecution_Sequence BIGINT,
	INOUT vCampaignId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CampaignName: ',IFNULL(vCampaignName,'NULL VALUE')
			,' | CampaignDesc: ',IFNULL(vCampaignDesc,'NULL VALUE')
			,' | CampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			,' | CLMSegmentId: ',IFNULL(vCLMSegmentId,'NULL VALUE')
			,' | CampaignCGPercentage: ',IFNULL(vCampaignCGPercentage,'NULL VALUE')
			,' | CampaignResponseDays: ',IFNULL(vCampaignResponseDays,'NULL VALUE')
			,' | CampaignState: ',IFNULL(vCampaignState,'NULL VALUE')
			,' | CampaignSmartPtyState: ',IFNULL(vCampaignSmartPtyState,'NULL VALUE')
			,' | CampaignIsOnDemand: ',IFNULL(vCampaignIsOnDemand,'NULL VALUE')
			,' | CampaignUsesWaterfall: ',IFNULL(vCampaignUsesWaterfall,'NULL VALUE')
			,' | CampaignIsPerpetual: ',IFNULL(vCampaignIsPerpetual,'NULL VALUE')
			,' | CampaignStartDate: ',IFNULL(vCampaignStartDate,'NULL VALUE')
			,' | CampaignEndDate: ',IFNULL(vCampaignEndDate,'NULL VALUE')
			,' | Execution_Sequence: ',IFNULL(vExecution_Sequence,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
		SET vErrMsg = CONCAT('CampaignThemeId is not valid.');
		SET vSuccess = 0;
	END IF;

	IF vCLMSegmentId IS NULL OR vCLMSegmentId <= 0 THEN
		SET vErrMsg = CONCAT('Life Cycle Segment is not valid.');
		SET vSuccess = 0;
	END IF;

	IF vExecution_Sequence IS NULL OR vExecution_Sequence <= 0 THEN
		SELECT MAX(Execution_Sequence) INTO vExecution_Sequence FROM CLM_Campaign;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Campaign(
			vUserId,
			vCampaignName,
		IFNULL(vCampaignDesc, ''),
			vCampaignThemeId,
			vCLMSegmentId,
		IFNULL(vCampaignCGPercentage, 0),
		IFNULL(vCampaignResponseDays ,0),
		IFNULL(vCampaignState, 0),
		IFNULL(vCampaignSmartPtyState, 0),
		IFNULL(vCampaignIsOnDemand, 0),
			IFNULL(vCampaignUsesWaterfall, 1),
			IFNULL(vCampaignIsPerpetual, 1),
			IFNULL(vCampaignStartDate, CURRENT_DATE()),
			IFNULL(vCampaignEndDate, '9999-12-31'),
			vExecution_Sequence,
		vCampaignId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Campaign_Events`(
	IN vUserId BIGINT, 
	IN vEventName VARCHAR(1024),
	IN vExtEventName VARCHAR(1024),
	IN vCampaignThemeId BIGINT,
	IN vCampaignId BIGINT, 
	IN vCLMSegmentId BIGINT, 
	IN vCampTriggerId BIGINT, 
	IN vChannelId BIGINT, 
	IN vMicroSegmentId BIGINT, 
	IN vCreativeId BIGINT, 
	IN vExecution_Sequence BIGINT, 
	INOUT vEventId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | vEventName: ',IFNULL(vEventName,'NULL VALUE')
			,' | vExtEventName: ',IFNULL(vExtEventName,'NULL VALUE')
			,' | vCampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			,' | vCampaignId: ',IFNULL(vCampaignId,'NULL VALUE')
			,' | vCLMSegmentId: ',IFNULL(vCLMSegmentId,'NULL VALUE')
			,' | vCampTriggerId: ',IFNULL(vCampTriggerId,'NULL VALUE')
			,' | vChannelId: ',IFNULL(vChannelId,'NULL VALUE')
			,' | vMicroSegmentId: ',IFNULL(vMicroSegmentId,'NULL VALUE')
			,' | vCreativeId: ',IFNULL(vCreativeId,'NULL VALUE')
			,' | vExecution_Sequence: ',IFNULL(vExecution_Sequence,'NULL VALUE')
			,' | vEventId: ',IFNULL(vEventId,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vEventName IS NULL OR vEventName = '' THEN
		SET vErrMsg = CONCAT('vEventName cannot be empty.');
		SET vSuccess = 0;
	END IF;
	IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampaignThemeId.');
		SET vSuccess = 0;
	END IF;
	IF vCampaignId IS NULL OR vCampaignId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampaignId');
		SET vSuccess = 0;
	END IF;
	IF vCLMSegmentId IS NULL OR vCLMSegmentId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CLMSegmentId');
		SET vSuccess = 0;
	END IF;
	IF vCampTriggerId IS NULL OR vCampTriggerId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampTriggerId');
		SET vSuccess = 0;
	END IF;
	IF vChannelId IS NULL OR vChannelId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid ChannelId');
		SET vSuccess = 0;
	END IF;
	IF vMicroSegmentId IS NULL OR vMicroSegmentId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid MicroSegmentId');
		SET vSuccess = 0;
	END IF;
	IF vCreativeId IS NULL OR vCreativeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CreativeId');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Campaign_Events(
			vUserId, 
		IFNULL(vEventName,''),
		IFNULL(vExtEventName,''),
		IFNULL(vCampaignThemeId ,0),
		IFNULL(vCampaignId ,0), 
		IFNULL(vCLMSegmentId ,0), 
		IFNULL(vCampTriggerId ,0), 
		IFNULL(vChannelId ,0), 
		IFNULL(vMicroSegmentId ,0), 
		IFNULL(vCreativeId ,0), 
		IFNULL(vExecution_Sequence ,0), 
		vEventId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Campaign_Trigger`(
	IN vUserId BIGINT, 
	IN vCampTriggerName VARCHAR(128), 
	IN vCampTriggerDesc VARCHAR(1024), 
	IN vPrecedingTriggerId BIGINT,
	IN vDaysOffsetFromPrecedingTrigger SMALLINT,
	IN vCTState TINYINT,
	IN vCTUsesGoodTimeModel	TINYINT,
	IN vCTIsMandatory TINYINT,
	IN vIsAReminder TINYINT,
	IN vCampaignThemeId BIGINT,
	IN vCampaignId BIGINT,
	IN vCTReplacedQueryAsInUI TEXT,
	IN vCTReplacedQuery TEXT,
	IN vIsValidSVOCCondition TINYINT,
	IN vCTReplacedQueryBillAsInUI TEXT,
	IN vCTReplacedQueryBill TEXT,
	IN vIsValidBillCondition TINYINT,
	IN vExecution_Sequence BIGINT,
	INOUT vCampTriggerId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CampTriggerName: ',IFNULL(vCampTriggerName,'NULL VALUE')
			,' | CampTriggerDesc: ',IFNULL(vCampTriggerDesc,'NULL VALUE')
			,' | PrecedingTriggerId: ',IFNULL(vPrecedingTriggerId,'NULL VALUE')
			,' | DaysOffsetFromPrecedingTrigger: ',IFNULL(vDaysOffsetFromPrecedingTrigger,'NULL VALUE')
			,' | CTState: ',IFNULL(vCTState,'NULL VALUE')
			,' | CTUsesGoodTimeModel: ',IFNULL(vCTUsesGoodTimeModel,'NULL VALUE')
			,' | CTIsMandatory: ',IFNULL(vCTIsMandatory,'NULL VALUE')
			,' | IsAReminder: ',IFNULL(vIsAReminder,'NULL VALUE')
			,' | CampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			,' | CampaignId: ',IFNULL(vCampaignId,'NULL VALUE')
			,' | CTReplacedQueryAsInUI: ',IFNULL(vCTReplacedQueryAsInUI,'NULL VALUE')
			,' | CTReplacedQuery: ',IFNULL(vCTReplacedQuery,'NULL VALUE')
			,' | IsValidSVOCCondition: ',IFNULL(vIsValidSVOCCondition,'NULL VALUE')
			,' | CTReplacedQueryBillAsInUI: ',IFNULL(vCTReplacedQueryBillAsInUI,'NULL VALUE')
			,' | CTReplacedQueryBill: ',IFNULL(vCTReplacedQueryBill,'NULL VALUE')
			,' | IsValidBillCondition: ',IFNULL(vIsValidBillCondition,'NULL VALUE')
			,' | Execution_Sequence: ',IFNULL(vExecution_Sequence,'NULL VALUE')
			,' | CampTriggerId: ',IFNULL(vCampTriggerId,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampTriggerName IS NULL OR vCampTriggerName = '' THEN
		SET vErrMsg = CONCAT('CampTriggerName cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampaignThemeId IS NULL OR vCampaignThemeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampaignThemeId.');
		SET vSuccess = 0;
	END IF;

	IF vCampaignId IS NULL OR vCampaignId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CampaignId.');
		SET vSuccess = 0;
	END IF;

	IF vExecution_Sequence IS NULL OR vExecution_Sequence <= 0 THEN
		SELECT MAX(Execution_Sequence) INTO vExecution_Sequence FROM CLM_Campaign_Trigger;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Campaign_Trigger(
			vUserId, 
			vCampTriggerName, 
		IFNULL(vCampTriggerDesc,''),
		IFNULL(vPrecedingTriggerId ,0),
		IFNULL(vDaysOffsetFromPrecedingTrigger,0),
		IFNULL(vCTState, 0),
		IFNULL(vCTUsesGoodTimeModel,0),
		IFNULL(vCTIsMandatory, 0),
		IFNULL(vIsAReminder, 0),
			vCampaignThemeId,
			vCampaignId,
		IFNULL(vCTReplacedQueryAsInUI,''),
		IFNULL(vCTReplacedQuery,''),
		IFNULL(vIsValidSVOCCondition, 0),
		IFNULL(vCTReplacedQueryBillAsInUI,''),
		IFNULL(vCTReplacedQueryBill,''),
		IFNULL(vIsValidBillCondition, 0),
			IFNULL(vExecution_Sequence ,1),
		vCampTriggerId,
		vErrMsg,
		vSuccess
	); 
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_CampaignTheme`(
	IN vUserId BIGINT, 
	IN vCampaignThemeName VARCHAR(128), 
	IN vCampaignThemeDesc VARCHAR(1024), 
	IN vExecution_Sequence BIGINT, 
	INOUT vCampaignThemeId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
            'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CampaignThemeName: ',IFNULL(vCampaignThemeName,'NULL VALUE')
			,' | CampaignThemeDesc: ',IFNULL(vCampaignThemeDesc,'NULL VALUE')
			,' | Execution_Sequence: ',IFNULL(vExecution_Sequence,'NULL VALUE')
			,' | CampaignThemeId: ',IFNULL(vCampaignThemeId,'NULL VALUE')
			);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCampaignThemeName IS NULL OR vCampaignThemeName = '' THEN
		SET vErrMsg = CONCAT('Campaign Theme Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vExecution_Sequence IS NULL OR vExecution_Sequence <= 0 THEN
		SELECT MAX(Execution_Sequence) INTO vExecution_Sequence FROM CLM_CampaignTheme;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_CampaignTheme(
		vUserId,
		vCampaignThemeName,
		IFNULL(vCampaignThemeDesc, ''),
		vExecution_Sequence,
		vCampaignThemeId,
		vErrMsg,
		vSuccess
	);
	END IF;

	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Channel`(
	IN vUserId BIGINT,
	IN vChannelName VARCHAR(128),
	IN vChannelDesc VARCHAR(1024),
	INOUT vChannelId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | ChannelName: ',IFNULL(vChannelName,'NULL VALUE')
			,' | ChannelDesc: ',IFNULL(vChannelDesc,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vChannelName IS NULL OR vChannelName = '' THEN
		SET vErrMsg = CONCAT('Channel Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Channel(
			vUserId,
			vChannelName,
		IFNULL(vChannelDesc, ''),
		vChannelId,
		vErrMsg,
		vSuccess
	); 
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Creative`(
	IN vUserId BIGINT,
	IN vCreativeName VARCHAR(128),
	IN vCreativeDesc VARCHAR(1024),
	IN vExtCreativeKey VARCHAR(128), 
	IN vChannelId BIGINT,
	IN vCreative TEXT,
	IN vHeader TEXT,
	IN vTemplatePersonalizationScore TINYINT,
	IN vOfferId BIGINT,
	INOUT vCreativeId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CreativeName: ',IFNULL(vCreativeName,'NULL VALUE')
			,' | CreativeDesc: ',IFNULL(vCreativeDesc,'NULL VALUE')
			,' | ExtCreativeKey: ',IFNULL(vExtCreativeKey,'NULL VALUE')
			,' | ChannelId: ',IFNULL(vChannelId,'NULL VALUE')
			,' | Creative: ',IFNULL(vCreative,'NULL VALUE')
			,' | Header: ',IFNULL(vHeader,'NULL VALUE')
			,' | TemplatePersonalizationScore: ',IFNULL(vTemplatePersonalizationScore,'NULL VALUE')
			,' | OfferId: ',IFNULL(vOfferId,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCreativeName IS NULL OR vCreativeName = '' THEN
		SET vErrMsg = CONCAT('CreativeName cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vChannelId IS NULL OR vChannelId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid ChannelId.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Creative(
			vUserId,
		IFNULL(vCreativeName,''),
		IFNULL(vCreativeDesc,''),
		IFNULL(vExtCreativeKey,''), 
			vChannelId,
		IFNULL(vCreative,''),
		IFNULL(vHeader,''),
		IFNULL(vTemplatePersonalizationScore, 0),
		IFNULL(vOfferId ,0),
		vCreativeId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Creative_PFields`(
	IN vUserId BIGINT, 
	IN vCreativeId BIGINT ,
	IN vPFieldName VARCHAR(64) ,
	IN vPFieldView VARCHAR(64) ,
	IN vPFieldColumn VARCHAR(64) ,
	IN vPFieldDefaultValue VARCHAR(1024) ,
	IN vPFieldAttributes TEXT,
	IN vPFieldQualConditionAsInUI TEXT,
	IN vPFieldQualConditionQuery TEXT,
	IN vPFieldRecoChecksAsInUI TEXT,
	IN vPFieldRecoChecksAsInUIQuery TEXT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CreativeId: ',IFNULL(vCreativeId,'NULL VALUE')
			,' | PFieldName: ',IFNULL(vPFieldName,'NULL VALUE')
			,' | PFieldView: ',IFNULL(vPFieldView,'NULL VALUE')
			,' | PFieldColumn: ',IFNULL(vPFieldColumn,'NULL VALUE')
			,' | PFieldDefaultValue: ',IFNULL(vPFieldDefaultValue,'NULL VALUE')
			,' | PFieldAttributes: ',IFNULL(vPFieldAttributes,'NULL VALUE')
			,' | PFieldQualConditionAsInUI: ',IFNULL(vPFieldQualConditionAsInUI,'NULL VALUE')
			,' | PFieldQualConditionQuery: ',IFNULL(vPFieldQualConditionQuery,'NULL VALUE')
			,' | PFieldRecoChecksAsInUI: ',IFNULL(vPFieldRecoChecksAsInUI,'NULL VALUE')
			,' | PFieldRecoChecksAsInUIQuery: ',IFNULL(vPFieldRecoChecksAsInUIQuery,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCreativeId IS NULL OR vCreativeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CreativeId.');
		SET vSuccess = 0;
	END IF;

	IF vPFieldName IS NULL OR vPFieldName = '' THEN
		SET vErrMsg = CONCAT('Personalization Field Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Creative_PFields(
			vUserId, 
			vCreativeId,
			vPFieldName,
		IFNULL(vPFieldView,''),
		IFNULL(vPFieldColumn,''),
		IFNULL(vPFieldDefaultValue,''),
		IFNULL(vPFieldAttributes,''),
		IFNULL(vPFieldQualConditionAsInUI,''),
		IFNULL(vPFieldQualConditionQuery,''),
		IFNULL(vPFieldRecoChecksAsInUI,''),
		IFNULL(vPFieldRecoChecksAsInUIQuery,''),
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Exclusion_Filter`(
	IN vUserId BIGINT,
	IN vEFName VARCHAR(128),
	IN vEFDesc VARCHAR(1024),
	IN vEFChannelId BIGINT,
	IN vEFTReplacedQueryAsInUI TEXT,
	IN vEFReplacedQuery TEXT,
	IN vIsValidSVOCCondition TINYINT,
	IN vEFReplacedQueryBillAsInUI TEXT,
	IN vEFReplacedQueryBill TEXT,
	IN vIsValidBillCondition TINYINT,
	IN vEFState TINYINT,
	IN vEFSequence BIGINT,
	INOUT vEFId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | vEFName: ',IFNULL(vEFName,'NULL VALUE')
			,' | vEFDesc: ',IFNULL(vEFDesc,'NULL VALUE')
			,' | vEFChannelId: ',IFNULL(vEFChannelId,'NULL VALUE')
			,' | vEFTReplacedQueryAsInUI: ',IFNULL(vEFTReplacedQueryAsInUI,'NULL VALUE')
			,' | vEFReplacedQuery: ',IFNULL(vEFReplacedQuery,'NULL VALUE')
			,' | vIsValidSVOCCondition: ',IFNULL(vIsValidSVOCCondition,'NULL VALUE')
			,' | vEFReplacedQueryBillAsInUI: ',IFNULL(vEFReplacedQueryBillAsInUI,'NULL VALUE')
			,' | vEFReplacedQueryBill: ',IFNULL(vEFReplacedQueryBill,'NULL VALUE')
			,' | vIsValidBillCondition: ',IFNULL(vIsValidBillCondition,'NULL VALUE')
			,' | vEFState: ',IFNULL(vEFState,'NULL VALUE')
			,' | vEFSequence: ',IFNULL(vEFSequence,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vEFName IS NULL OR vEFName = '' THEN
		SET vErrMsg = CONCAT('Exclusion Filter Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vEFChannelId IS NULL OR vEFChannelId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid ChannelId');
		SET vSuccess = 0;
	END IF;

	IF vEFSequence IS NULL OR vEFSequence <= 0 THEN
		SELECT MAX(EFSequence)INTO vEFSequence FROM CLM_Exclusion_Filter;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Exclusion_Filter(
			vUserId,
			vEFName,
		IFNULL(vEFDesc,''),
			vEFChannelId,
		IFNULL(vEFTReplacedQueryAsInUI,''),
		IFNULL(vEFReplacedQuery,''),
		IFNULL(vIsValidSVOCCondition, 0),
		IFNULL(vEFReplacedQueryBillAsInUI,''),
		IFNULL(vEFReplacedQueryBill,''),
		IFNULL(vIsValidBillCondition, 0),
		IFNULL(vEFState, 0),
		IFNULL(vEFSequence ,0),
		vEFId,
		vErrMsg,
		vSuccess
	);
	END IF;
	
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_MicroSegment`(
	IN vUserId BIGINT,
	IN vMicroSegmentName VARCHAR(128),
	IN vMicroSegmentDesc VARCHAR(1024),
	IN vMSReplacedQueryAsInUI TEXT,
	IN vMSReplacedQuery TEXT,
	IN vIsValidSVOCCondition TINYINT,
	IN vMSReplacedQueryBillAsInUI TEXT,
	IN vMSReplacedQueryBill TEXT,
	IN vIsValidBillCondition TINYINT,
	INOUT vMicroSegmentId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | MicroSegmentName: ',IFNULL(vMicroSegmentName,'NULL VALUE')
			,' | MicroSegmentDesc: ',IFNULL(vMicroSegmentDesc,'NULL VALUE')
			,' | MSReplacedQueryAsInUI: ',IFNULL(vMSReplacedQueryAsInUI,'NULL VALUE')
			,' | MSReplacedQuery: ',IFNULL(vMSReplacedQuery,'NULL VALUE')
			,' | IsValidSVOCCondition: ',IFNULL(vIsValidSVOCCondition,'NULL VALUE')
			,' | MSReplacedQueryBillAsInUI: ',IFNULL(vMSReplacedQueryBillAsInUI,'NULL VALUE')
			,' | MSReplacedQueryBill: ',IFNULL(vMSReplacedQueryBill,'NULL VALUE')
			,' | IsValidBillCondition: ',IFNULL(vIsValidBillCondition,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vMicroSegmentName IS NULL OR vMicroSegmentName = '' THEN
		SET vErrMsg = CONCAT('Microsegment Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_MicroSegment(
			vUserId,
			vMicroSegmentName,
		IFNULL(vMicroSegmentDesc, ''),
		IFNULL(vMSReplacedQueryAsInUI, ''),
		IFNULL(vMSReplacedQuery, ''),
		IFNULL(vIsValidSVOCCondition, 0),
		IFNULL(vMSReplacedQueryBillAsInUI, ''),
		IFNULL(vMSReplacedQueryBill, ''),
		IFNULL(vIsValidBillCondition, 0),
		vMicroSegmentId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Offer`(
	IN vUserId BIGINT,
	IN vOfferName VARCHAR(128),
	IN vOfferDesc VARCHAR(1024),
	IN vOfferTypeId BIGINT,
	IN vOfferDef VARCHAR(2048),
	IN vOfferStartDate DATE, 
	IN vOfferEndDate DATE, 
	IN vOfferRedemptionDays INTEGER,
	IN vOfferIsAtCustomerLevel TINYINT,
	INOUT vOfferId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | OfferName: ',IFNULL(vOfferName,'NULL VALUE')
			,' | OfferDesc: ',IFNULL(vOfferDesc,'NULL VALUE')
			,' | OfferTypeId: ',IFNULL(vOfferTypeId,'NULL VALUE')
			,' | OfferDef: ',IFNULL(vOfferDef,'NULL VALUE')
			,' | OfferStartDate: ',IFNULL(vOfferStartDate,'NULL VALUE')
			,' | OfferEndDate: ',IFNULL(vOfferEndDate,'NULL VALUE')
			,' | OfferRedemptionDays: ',IFNULL(vOfferRedemptionDays,'NULL VALUE')
			,' | OfferIsAtCustomerLevel: ',IFNULL(vOfferIsAtCustomerLevel,'NULL VALUE')
			,' | OfferId: ',IFNULL(vOfferId,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vOfferName IS NULL OR vOfferName = '' THEN
		SET vErrMsg = CONCAT('Offer Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vOfferTypeId IS NULL OR vOfferTypeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid OfferTypeId.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Offer(
			vUserId,
		IFNULL(vOfferName, ''),
		IFNULL(vOfferDesc, ''),
		IFNULL(vOfferTypeId ,0),
		IFNULL(vOfferDef,''),
			IFNULL(vOfferStartDate,CURRENT_DATE()),
			IFNULL(vOfferEndDate,'9999-12-31'),
		IFNULL(vOfferRedemptionDays,0),
		IFNULL(vOfferIsAtCustomerLevel, 0),
		vOfferId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_OfferType`(
	IN vUserId BIGINT,
	IN vOfferTypeDesc VARCHAR(1024),
	IN vOfferTypeName VARCHAR(128),
	INOUT vOfferTypeId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | OfferTypeName: ',IFNULL(vOfferTypeName,'NULL VALUE')
			,' | OfferTypeDesc: ',IFNULL(vOfferTypeDesc,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vOfferTypeName IS NULL OR vOfferTypeName = '' THEN
		SET vErrMsg = CONCAT('OfferType Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_OfferType(
			vUserId,
		IFNULL(vOfferTypeDesc, ''),
		IFNULL(vOfferTypeName, ''),
		vOfferTypeId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Reco_In_Creative`(
	IN vUserId BIGINT,
	IN vCreativeId BIGINT ,
	IN vExclude_Recommended_SameAP_NotLT_FavCat TINYINT,
	IN vExclude_Recommended_SameLTD_FavCat TINYINT,
	IN vExclude_Recommended_SameLT_FavCat TINYINT,
	IN vRecommendation_Column VARCHAR(2048), 
	IN vRecommendation_Type VARCHAR(2048),
	IN vExclude_Stock_out TINYINT,
	IN vExclude_Transaction_X_Days TINYINT,
	IN vExclude_Recommended_X_Days TINYINT,
	IN vExclude_Never_Brought TINYINT,
	IN vExclude_No_Image TINYINT,
	IN vNumber_Of_Recommendation INTEGER,
	IN vRecommendation_Filter_Logic VARCHAR(2048),
	IN vRecommendation_Column_Header VARCHAR(2048),
	IN vIs_Recommended_SameLT_FavCat TINYINT,
	IN vIs_Recommended_SameLTD_Cat TINYINT,
	IN vIs_Recommended_SameLTD_Dep TINYINT,
	IN vIs_Recommended_NewLaunch_Product TINYINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT

)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CreativeId: ',IFNULL(vCreativeId,'NULL VALUE')
			,' | Exclude_Recommended_SameAP_NotLT_FavCat: ',IFNULL(vExclude_Recommended_SameAP_NotLT_FavCat,'NULL VALUE')
			,' | Exclude_Recommended_SameLTD_FavCat: ',IFNULL(vExclude_Recommended_SameLTD_FavCat,'NULL VALUE')
			,' | Exclude_Recommended_SameLT_FavCat: ',IFNULL(vExclude_Recommended_SameLT_FavCat,'NULL VALUE')
			,' | Recommendation_Column: ',IFNULL(vRecommendation_Column,'NULL VALUE')
			,' | Recommendation_Type: ',IFNULL(vRecommendation_Type,'NULL VALUE')
			,' | Exclude_Stock_out: ',IFNULL(vExclude_Stock_out,'NULL VALUE')
			,' | Exclude_Transaction_X_Days: ',IFNULL(vExclude_Transaction_X_Days,'NULL VALUE')
			,' | Exclude_Recommended_X_Days: ',IFNULL(vExclude_Recommended_X_Days,'NULL VALUE')
			,' | Exclude_Never_Brought: ',IFNULL(vExclude_Never_Brought,'NULL VALUE')
			,' | Exclude_No_Image: ',IFNULL(vExclude_No_Image,'NULL VALUE')
			,' | Number_Of_Recommendation: ',IFNULL(vNumber_Of_Recommendation,'NULL VALUE')
			,' | Recommendation_Filter_Logic: ',IFNULL(vRecommendation_Filter_Logic,'NULL VALUE')
			,' | Recommendation_Column_Header: ',IFNULL(vRecommendation_Column_Header,'NULL VALUE')
			,' | Is_Recommended_SameLT_FavCat: ',IFNULL(vIs_Recommended_SameLT_FavCat,'NULL VALUE')
			,' | Is_Recommended_SameLTD_Cat: ',IFNULL(vIs_Recommended_SameLTD_Cat,'NULL VALUE')
			,' | Is_Recommended_SameLTD_Dep: ',IFNULL(vIs_Recommended_SameLTD_Dep,'NULL VALUE')
			,' | Is_Recommended_NewLaunch_Product: ',IFNULL(vIs_Recommended_NewLaunch_Product,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCreativeId IS NULL OR vCreativeId <= 0 THEN
		SET vErrMsg = CONCAT('Invalid CreativeId.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Reco_In_Creative(
			vUserId,
			vCreativeId,
		IFNULL(vExclude_Recommended_SameAP_NotLT_FavCat, 0),
		IFNULL(vExclude_Recommended_SameLTD_FavCat, 0),
		IFNULL(vExclude_Recommended_SameLT_FavCat, 0),
		IFNULL(vRecommendation_Column,''),
		IFNULL(vRecommendation_Type,''),
		IFNULL(vExclude_Stock_out, 0),
		IFNULL(vExclude_Transaction_X_Days, 0),
		IFNULL(vExclude_Recommended_X_Days, 0),
		IFNULL(vExclude_Never_Brought, 0),
		IFNULL(vExclude_No_Image, 0),
		IFNULL(vNumber_Of_Recommendation, 0),
		IFNULL(vRecommendation_Filter_Logic,''),
		IFNULL(vRecommendation_Column_Header,''),
		IFNULL(vIs_Recommended_SameLT_FavCat, 0),
		IFNULL(vIs_Recommended_SameLTD_Cat, 0),
		IFNULL(vIs_Recommended_SameLTD_Dep, 0),
		IFNULL(vIs_Recommended_NewLaunch_Product, 0),
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_Segment`(
	IN vUserId BIGINT,
	IN vCLMSegmentName VARCHAR(128),
	IN vCLMSegmentDesc VARCHAR(1024),
	IN vCLMSegmentReplacedQueryAsInUI TEXT,
	IN vCLMSegmentReplacedQuery TEXT,
	IN vIsValidCondition TINYINT,
	INOUT vCLMSegmentId BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
			'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | CLMSegmentName: ',IFNULL(vCLMSegmentName,'NULL VALUE')
			,' | CLMSegmentDesc: ',IFNULL(vCLMSegmentDesc,'NULL VALUE')
			,' | CLMSegmentReplacedQueryAsInUI: ',IFNULL(vCLMSegmentReplacedQueryAsInUI,'NULL VALUE')
			,' | CLMSegmentReplacedQuery: ',IFNULL(vCLMSegmentReplacedQuery,'NULL VALUE')
			,' | IsValidCondition: ',IFNULL(vIsValidCondition,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId <= 0 THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCLMSegmentName IS NULL OR vCLMSegmentName = '' THEN
		SET vErrMsg = CONCAT('Segment Name cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
	CALL PROC_CLM_CU_CLM_Segment(
			vUserId,
			vCLMSegmentName,
		IFNULL(vCLMSegmentDesc, ''),
		IFNULL(vCLMSegmentReplacedQueryAsInUI, ''),
		IFNULL(vCLMSegmentReplacedQuery, ''),
		IFNULL(vIsValidCondition, -1),
		vCLMSegmentId,
		vErrMsg,
		vSuccess
	);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;

END


|
CREATE or replace PROCEDURE `PROC_UI_CU_CLM_User`(
	IN vUserId VARCHAR(128), 
	IN vCreator_SOLUS_UID BIGINT,  
	INOUT vSOLUS_UID BIGINT,
	OUT vErrMsg TEXT,
	OUT vSuccess TINYINT
)
BEGIN
	DECLARE vResult TINYINT;
	DECLARE vInputValues TEXT;
	
	SET vSuccess = 1;
	SET vErrMsg = '';

	SET vInputValues  = CONCAT(
            'UserId: ', IFNULL(vUserId,'NULL VALUE')
			,' | Creator_SOLUS_UID: ',IFNULL(vCreator_SOLUS_UID,'NULL VALUE')
			,' | SOLUS_UID: ',IFNULL(vSOLUS_UID,'NULL VALUE')
		);

	IF vUserId IS NULL OR vUserId = '' THEN
		SET vErrMsg = CONCAT('User Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vCreator_SOLUS_UID IS NULL OR vCreator_SOLUS_UID <= 0 THEN
		SET vErrMsg = CONCAT('Creator Id cannot be empty.');
		SET vSuccess = 0;
	END IF;

	IF vSuccess = 1 THEN 
		CALL PROC_CLM_CU_CLM_User(vUserId, IFNULL(vCreator_SOLUS_UID,0), vSOLUS_UID, vErrMsg,vSuccess);
	END IF;
	IF vSuccess = 0 THEN 
		SET vErrMsg = CONCAT(vErrMsg , '\r\n', vInputValues);	
	END IF;
END


|
CREATE or replace PROCEDURE `PROC_CLM_UI_Campign_Bulk_Update`(
	IN vEventId BIGINT,
	IN vCTState BIGINT,
    IN vExecution_Sequence BIGINT,
    IN vCampaignStartDate BIGINT,
    IN vCampaignEndDate BIGINT
)
Begin


SET SQL_SAFE_UPDATES=0;



if vCTState=0 then
Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set CTState = 0
where A.EventId=vEventId and A.CampTriggerId =B.CampTriggerId;

end if;

if vCTState=1 then
Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set CTState = 1
where A.EventId=vEventId and A.CampTriggerId =B.CampTriggerId;
end if;

if vCTState=2 then
Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set CTState = 2
where A.EventId=vEventId and A.CampTriggerId =B.CampTriggerId;

end if;




Update CLM_Campaign_Events
set Execution_Sequence = vExecution_Sequence
where EventId=vEventId ;

Update CLM_Campaign_Events A, CLM_Campaign_Trigger B
set B.Execution_Sequence = vExecution_Sequence
where A.CampTriggerId=B.CampTriggerId and A.EventId=vEventId ;




Update  CLM_Campaign A, CLM_Campaign_Events B
set CampaignStartDate=vCampaignStartDate
where A.CampaignId=B.CampaignId and B.EventId=vEventId;




Update  CLM_Campaign A, CLM_Campaign_Events B
set CampaignEndDate=vCampaignEndDate
where A.CampaignId=B.CampaignId and B.EventId=vEventId;







END

